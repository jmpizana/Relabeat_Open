﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: 
 * 1.- Correct sample generation and placement inside the selected folder
 * 2.- http://va.lent.in/unity-make-your-lists-functional-with-reorderablelist/
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using PizanaUtility;

namespace MusicGeneration
{

    public class SampleLoaderWindow : EditorWindow
    {
        public string sourcePath, destinyPath;

        bool processing = false;
        float processingProgress = 0f;
        OnLoadingParameters loadingParameters;

        #region GUI

        [MenuItem("Window/SampleProcessor")]
        public static void ShowWindow()
        {
            GetWindow(typeof(SampleLoaderWindow), false, "Sample Loader");
        }

        private void OnGUI()
        {
            if (processing)
            {
                GUI.enabled = false;
                EditorUtility.DisplayProgressBar("Packaging Samples", "", processingProgress);
            }
            else GUI.enabled = true;

            Credits();

            PathSetting();
            ParametersSetting();
            Processing();

        }

        private void Processing()
        {
            if (sourcePath != null && sourcePath != "" && destinyPath != null && destinyPath != "" && GUILayout.Button("Load Samples"))
            {
                if (!CheckCorrectSourceSubdirectories())
                {
                    if (EditorUtility.DisplayDialog("Invalid folder structure", "Folder structure is not correct", "Generate folder structure", "Cancel"))
                    {
                        GenerateFolderStructure();
                    }
                    else
                    {
                        //Nothing for now
                    }
                    return;
                }

                //if (!CheckLayerListIntegrity())
                //{
                //    EditorUtility.DisplayDialog("Incorrect Layer number", "Layer number is different from available musical layers", "OK");
                //    return;
                //}

                LoadSamples();

            }
        }

        private void ParametersSetting()
        {
            SerializedObject so = new SerializedObject(this);


            so.ApplyModifiedProperties();
        }

        private void Credits()
        {
            GUIStyle style = new GUIStyle();
            style.fontStyle = FontStyle.Bold;
            style.fontSize = 15;

            EditorGUILayout.LabelField("Procedural Music Sample Loader", style);
            EditorGUILayout.LabelField("José maría Pizana García, 2018", style);
            EditorGUILayout.LabelField("www.jmpizana.com", style);

        }

        private void PathSetting()
        {


            //INPUT****************************************************************************************************
            EditorGUILayout.LabelField("Raw audio data parent folder:", GUILayout.ExpandWidth(false));
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("...", GUILayout.ExpandWidth(false), GUILayout.Width(30)))
            {
                string path = EditorUtility.OpenFolderPanel("Raw audio clips folder", Application.dataPath, "");

                if (CheckValidProjectPath(path)) sourcePath = path;
                else sourcePath = "";


            }

            GUI.enabled = false;
            GUILayout.TextField(sourcePath, GUILayout.ExpandWidth(true));
            GUI.enabled = true;

            EditorGUILayout.EndHorizontal();



            //OUTPUT******************************************************************************************************
            EditorGUILayout.LabelField("Output audio data parent folder:", GUILayout.ExpandWidth(false));
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("...", GUILayout.ExpandWidth(false), GUILayout.Width(30)))
            {
                string path = EditorUtility.OpenFolderPanel("Raw audio clips folder", Application.dataPath, "");

                if (CheckValidProjectPath(path)) destinyPath = path;
                else destinyPath = "";
            }

            GUI.enabled = false;
            GUILayout.TextField(destinyPath, GUILayout.ExpandWidth(true));
            GUI.enabled = true;

            EditorGUILayout.EndHorizontal();
        }

        #endregion

        #region Pre-Load
        private bool CheckValidProjectPath(string path)
        {
            if (path == null || path == "") return false;

            /*Uri uriPath = new Uri("file://" + path +"/"),
            parentPath = new Uri("file://" + Application.dataPath +"/");

            Debug.Log(uriPath.IsWellFormedOriginalString());
            Debug.Log(parentPath.IsWellFormedOriginalString());
            Debug.Log(parentPath.IsBaseOf(uriPath));
            Debug.Log(uriPath.IsBaseOf(parentPath));*/


            //Debug.Log(StringExtensions.IsSubPathOf(path, Application.dataPath));

            if (!(path + "/").Contains("/Resources/"))
            {
                EditorUtility.DisplayDialog("Invalid path", "Path selected is outside a Resources folder", "OK");
                return false;
            }
            else if (!StringExtensions.IsSubPathOf(path, Application.dataPath))
            {
                EditorUtility.DisplayDialog("Invalid path", "Path selected is outside project bounds", "OK");
                return false;
            }
            else if (path == Application.dataPath)
            {
                EditorUtility.DisplayDialog("Invalid path", "Path selected is the Assets folder", "OK");
                return false;
            }

            return true;

        }

        private bool CheckCorrectSourceSubdirectories()
        {

            DirectoryInfo mainFolder = new DirectoryInfo(sourcePath);

            string[] modeNames = MusicConstants.ModeNames;
            string[] layerNames = MusicConstants.LayerNames;
            int bmp;
            DirectoryInfo[] layerFolders = mainFolder.GetDirectories();
            if (!layerFolders.CompareFoldersWithNamesStrict(layerNames)) return false;

            for (int i = 0; i < layerFolders.Length; i++)
            {
                DirectoryInfo[] modeFolders = layerFolders[i].GetDirectories();

                if (!modeFolders.CompareFoldersWithNamesStrict(modeNames)) return false;

                for (int j = 0; j < modeFolders.Length; j++)
                {
                    DirectoryInfo[] bpmFolders = modeFolders[j].GetDirectories();

                    for (int k = 0; k < bpmFolders.Length; k++)
                    {
                        if (!int.TryParse(bpmFolders[k].Name, out bmp)) return false;

                        DirectoryInfo[] essentialityFolders = bpmFolders[j].GetDirectories();

                        if (!CheckEssentialityStructure(essentialityFolders)) return false;
                    }

                }
            }
            return true;
        }

        private bool CheckEssentialityStructure(DirectoryInfo[] essentialityFolders)
        {
            if (essentialityFolders.Length != 2f) return false;

            return essentialityFolders.CompareFoldersWithNamesStrict(MusicConstants.EssentialityDenominators);
        }

        /// <summary>
        /// Generates the source folder structure until Modes depth, because bmps and families are free
        ///We don't delete remaining extra folder in order to not remove important assets
        /// </summary>
        private void GenerateFolderStructure()
        {
            string[] modeNames = MusicConstants.ModeNames;
            string[] layerNames = MusicConstants.LayerNames;
            string layerPath, modePath, defaultBPMPath,
                essentialFolderTail = MusicConstants.EssentialityDenominators[0],
                nonEssentialFolderTail = MusicConstants.EssentialityDenominators[1];

            for (int i = 0; i < layerNames.Length; i++)
            {
                layerPath = sourcePath + "/" + layerNames[i];

                if (!Directory.Exists(layerPath)) Directory.CreateDirectory(layerPath);

                for (int j = 0; j < modeNames.Length; j++)
                {
                    modePath = layerPath + "/" + modeNames[j];
                    if (!Directory.Exists(modePath)) Directory.CreateDirectory(modePath);

                    //Generating sample bpm folder to harbor essentiality divisors
                    defaultBPMPath = modePath + "/140";
                    if (!Directory.Exists(defaultBPMPath)) Directory.CreateDirectory(defaultBPMPath);

                    //For this bpm we generate essential and non essential folders
                    if (!Directory.Exists(defaultBPMPath + "/" + essentialFolderTail)) Directory.CreateDirectory(defaultBPMPath + "/" + essentialFolderTail);
                    if (!Directory.Exists(defaultBPMPath + "/" + nonEssentialFolderTail)) Directory.CreateDirectory(defaultBPMPath + "/" + nonEssentialFolderTail);

                }
            }

            AssetDatabase.Refresh();
        }
        #endregion

        #region Sample Loading

        private void LoadSamples()
        {
            DirectoryInfo sourceDir = new DirectoryInfo(sourcePath), destinyDir = new DirectoryInfo(destinyPath);
            SampleLayerCollection mainLayerFamilies, baseLayerFamilies, accompanimentLayerFamilies;

            //First delete recursively the output folder, as we have to rebuild it
            destinyDir.Delete(true);
            destinyDir = Directory.CreateDirectory(destinyPath);

            loadingParameters = new OnLoadingParameters();

            LoadLayersFolders(sourceDir, out mainLayerFamilies, out baseLayerFamilies, out accompanimentLayerFamilies);

            EditorUtility.SetDirty(SampleLibrary.InstantiateCollection<SampleLibrary>(
                new LibraryParameters(mainLayerFamilies, baseLayerFamilies, accompanimentLayerFamilies),
                destinyPath + "/Libraries",
                "FullSampleLibrary"));

            Debug.Log("Sample library stored");
        }

        private void LoadLayersFolders(DirectoryInfo sourceDir, out SampleLayerCollection melodyLayer,
            out SampleLayerCollection baseLayer, out SampleLayerCollection accoLayer)
        {
            melodyLayer = new SampleLayerCollection();
            baseLayer = new SampleLayerCollection();
            accoLayer = new SampleLayerCollection();

            DirectoryInfo[] layerDirs = sourceDir.GetDirectories();
            DirectoryInfo layerDir;
            ModesDictionary generatedModeDictionary;

            for (int i = 0; i < layerDirs.Length; i++)
            {
                layerDir = layerDirs[i];
                loadingParameters.currentLayer = (MusicLayer)Enum.Parse(typeof(MusicLayer), layerDir.Name.ToUpper());


                string layerDestinyPath = destinyPath + "/" + layerDir.Name;
                LoadModesFolder(layerDir, layerDestinyPath, out generatedModeDictionary);

                SampleLayerCollection generatedLayer = SampleLayerCollection.InstantiateCollection<SampleLayerCollection>(
                    new LayerCollectionParams(loadingParameters.currentLayer, generatedModeDictionary),
                    layerDestinyPath,
                    layerDir.Name + " layer Collection"
                    );

                //We should get away with an overwrite
                switch (loadingParameters.currentLayer)
                {
                    case MusicLayer.BASE:
                        baseLayer = generatedLayer;
                        break;
                    case MusicLayer.HARMONY:
                        accoLayer = generatedLayer;
                        break;
                    case MusicLayer.MELODY:
                        melodyLayer = generatedLayer;
                        break;
                    case MusicLayer.ONESHOTS:
                        //TODO: it does not load oneshots for now
                        break;
                }
                //Debug.Log(currentLayer + " layer finished");
            }
        }

        private void LoadModesFolder(DirectoryInfo layerDir, string layerDestinypath, out ModesDictionary generatedModeDictionary)
        {
            generatedModeDictionary = new ModesDictionary();
            DirectoryInfo[] modesDirs = layerDir.GetDirectories();

            DirectoryInfo modeDir;
            string modeDestinyPath;
            BPMDictionary bpmsDictionary;

            for (int i = 0; i < modesDirs.Length; i++)
            {
                modeDir = modesDirs[i];
                modeDestinyPath = layerDestinypath + "/" + modeDir.Name;
                loadingParameters.currentMode = (MusicMode)Enum.Parse(typeof(MusicMode), modeDir.Name.ToUpper());


                LoadBPMSFolder(modeDir, modeDestinyPath, out bpmsDictionary);

                SampleModeCollection generatedMode = SampleModeCollection.InstantiateCollection<SampleModeCollection>(
                    new ModeCollectionParams(loadingParameters.currentMode, bpmsDictionary),
                    modeDestinyPath,
                    layerDir.Name + "'s " + modeDir.Name + " Data Collection"
                    );


                generatedModeDictionary.Add(loadingParameters.currentMode, generatedMode);
            }

        }

        private void LoadBPMSFolder(DirectoryInfo modeDir, string modeDestinyPath, out BPMDictionary generatedBPMDictionary)
        {
            generatedBPMDictionary = new BPMDictionary();
            DirectoryInfo[] bpmsDirs = modeDir.GetDirectories();

            DirectoryInfo bpmDir;
            string bpmDestinyPath;
            List<SampleFamily> essentialSampleFamilies, nonEssentialSampleFamilies;

            for (int i = 0; i < bpmsDirs.Length; i++)
            {
                bpmDir = bpmsDirs[i];
                bpmDestinyPath = modeDestinyPath + "/" + bpmDir.Name;
                loadingParameters.currentBPM = int.Parse(bpmDir.Name);

                //if (!int.TryParse(bpmDir.Name, out bpm)) throw new UnityException("Bpm directory name could not be parsed to an int");


                LoadEssentialityFolder(bpmDir, bpmDestinyPath, out essentialSampleFamilies, out nonEssentialSampleFamilies);
                //LoadFamiliesFolder(bpmDir, bpmDestinyPath, out sampleFamilies);

                generatedBPMDictionary.Add(loadingParameters.currentBPM, SampleBPMCollection.InstantiateCollection<SampleBPMCollection>(
                    new BPMCollectionParams(loadingParameters.currentBPM, nonEssentialSampleFamilies, essentialSampleFamilies),
                    bpmDestinyPath, bpmDir.Name + "bpmCollection"
                    ));
            }

        }

        private void LoadEssentialityFolder(DirectoryInfo bpmDir, string bpmDestinyPath,
            out List<SampleFamily> essentialSampleFamilies,
            out List<SampleFamily> nonEssentialSampleFamilies)
        {
            string[] essentialDenominators = MusicConstants.EssentialityDenominators;
            string essentialDestinyPath = bpmDestinyPath + "/" + essentialDenominators[0];
            string nonEssentialDestinyPath = bpmDestinyPath + "/" + essentialDenominators[1];

            DirectoryInfo[] essentialityDirs = bpmDir.GetDirectories();
            DirectoryInfo essentialDirectory = essentialityDirs.GetByName(essentialDenominators[0]);
            DirectoryInfo nonEssentialDirectory = essentialityDirs.GetByName(essentialDenominators[1]);

            LoadFamiliesFolder(essentialDirectory, essentialDestinyPath, out essentialSampleFamilies);
            LoadFamiliesFolder(nonEssentialDirectory, nonEssentialDestinyPath, out nonEssentialSampleFamilies);
        }

        private void LoadFamiliesFolder(DirectoryInfo essentialityDir, string essentialityDestinyPath, out List<SampleFamily> sampleFamilies)
        {
            sampleFamilies = new List<SampleFamily>();
            DirectoryInfo[] familiesDirs = essentialityDir.GetDirectories();

            DirectoryInfo familyDir;
            string familyDestinyPath;
            List<Sample> samples;

            for (int i = 0; i < familiesDirs.Length; i++)
            {
                familyDir = familiesDirs[i];
                familyDestinyPath = essentialityDestinyPath + "/" + familyDir.Name;

                LoadSamples(familyDir, familyDestinyPath, out samples);

                sampleFamilies.Add(SampleFamily.InstantiateCollection<SampleFamily>(
                    new FamilyCollectionParams(samples, loadingParameters.currentBPM, loadingParameters.currentMode, loadingParameters.currentRhythm),
                    familyDestinyPath,
                    familyDir.Name
                    ));
            }

        }

        private void LoadSamples(DirectoryInfo familyDir, string familyDestinyPath, out List<Sample> samples)
        {
            samples = new List<Sample>();
            string trimmedPath = familyDir.FullName.MakePathRelative(Application.dataPath + "/Resources/");

            AudioClip[] clips = Resources.LoadAll<AudioClip>(trimmedPath);
            Sample createdSample;

            for (int i = 0; i < clips.Length; i++)
            {
                AudioClip clip = clips[i];
                createdSample = Sample.InstantiateCollection<Sample>(
                    new SampleParameters(loadingParameters.currentBPM, clip, loadingParameters.currentLayer, loadingParameters.currentMode, loadingParameters.currentRhythm),
                    familyDestinyPath + "/Samples",
                    clip.name
                    );

                samples.Add(createdSample);
            }
        }

        private struct OnLoadingParameters
        {
            public int currentBPM;
            public MusicMode currentMode;
            public MusicRhythm currentRhythm;
            public MusicLayer currentLayer;

            public OnLoadingParameters(int currentBPM = -1, MusicMode currentMode = MusicMode.NONE,
                MusicRhythm currentRhythm = MusicRhythm._4_4, MusicLayer currentLayer = MusicLayer.ONESHOTS)
            {
                this.currentBPM = currentBPM;
                this.currentMode = currentMode;
                this.currentRhythm = currentRhythm;
                this.currentLayer = currentLayer;
            }

        }


        /*
                private void LoadBMPsFolders(DirectoryInfo layerDir, out MusicLayer currentLayer, out List<SampleFamilyCollection> generatedFamilies)
                {
                    DirectoryInfo[] bmpDirs = layerDir.GetDirectories();
                    //Debug.Log(layerDir.Name + ": " + bmpDirs.Length);

                    currentLayer = (MusicLayer)Enum.Parse(typeof(MusicLayer), layerDir.Name);
                    generatedFamilies = new List<SampleFamilyCollection>();

                    //Debug.Log("Current Layer: " + currentLayer);

                    for (int j = 0; j < bmpDirs.Length; j++)
                    {
                        DirectoryInfo bmpDir = bmpDirs[j];

                        int bmp = -1;

                        if (!int.TryParse(bmpDir.Name, out bmp))
                        {
                            Debug.Log("Bmp folder name could not be parsed");
                            throw new UnityException("Bmp folder name could not be parsed");
                        }

                        DirectoryInfo[] familiesDirs = bmpDir.GetDirectories();

                        for (int i = 0; i < familiesDirs.Length; i++)
                        {
                            DirectoryInfo familyDir = familiesDirs[i];
                            List<Sample> generatedFamily = new List<Sample>();

                            string trimmedPath = familyDir.FullName.MakePathRelative(Application.dataPath + "/Resources/");

                            AudioClip[] clips = Resources.LoadAll<AudioClip>(trimmedPath);
                            for (int k = 0; k < clips.Length; k++)
                            {
                                Sample instantiatedSample = Sample.InstantiateSample(bmp, clips[k], currentLayer,
                                    MusicalMode.NONE, MusicalRhythm._4_4,
                                    destinyPath + "/" + layerDir.Name + "/" + bmpDir.Name + "/" + familyDir.Name + "/", clips[k].name);

                                generatedFamily.Add(instantiatedSample);
                                EditorUtility.SetDirty(instantiatedSample);

                            }

                            SampleFamilyCollection instantiatedFamily = SampleFamilyCollection.InstantiateSampleFamily(generatedFamily, bmp,
                            MusicalMode.NONE, MusicalRhythm._4_4,
                            destinyPath + "/" + layerDir.Name + "/" + bmpDir.Name + "/", layerDir.Name + " " + bmpDir.Name + " " + familyDir.Name + " family");

                            generatedFamilies.Add(instantiatedFamily);
                            EditorUtility.SetDirty(instantiatedFamily);
                        }


                        //Debug.Log(bmpDir.Name + " bmp of layer " + layerDir.Name + " finished");
                    }
                }*/
        #endregion
    }
}
