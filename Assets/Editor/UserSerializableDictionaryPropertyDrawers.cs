﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TrackGeneration;
using MusicGeneration;
using PizanaUtility;

[CustomPropertyDrawer(typeof(StringStringDictionary))]
[CustomPropertyDrawer(typeof(ObjectColorDictionary))]
[CustomPropertyDrawer(typeof(ModesDictionary))]
[CustomPropertyDrawer(typeof(BPMDictionary))]
[CustomPropertyDrawer(typeof(TrackDangerDictionary))]
[CustomPropertyDrawer(typeof(ClipsDangerDictionary))]
public class AnySerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer {}
