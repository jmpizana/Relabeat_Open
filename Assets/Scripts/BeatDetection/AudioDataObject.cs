﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AudioAnalysis
{
    public class AudioDataObject : ScriptableObject
    {
        AudioData data;
        AudioClip clip;


    }

    [System.Serializable]
    public struct AudioData
    {
        public float bpm;
        public float[] eventTimes;

        public AudioData(float bpm, float[] eventTimes)
        {
            this.bpm = bpm;
            this.eventTimes = eventTimes;
        }
    }
}


