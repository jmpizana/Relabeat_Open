﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: 
 * set max number of together gaps
 * 
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MusicGeneration;
using PizanaUtility;
using Random = UnityEngine.Random;
using System.Threading.Tasks;

namespace TrackGeneration
{
    public class TrackGenerator
    {
        //TODO: In order to generate the track with the corresponding distances to the beat, player's speed has to be stored as 
        //a global variable, probably on a scriptableobject from which all necessary elements must extract data from

        TrackGenerationProperties parameters;
        TrackManager manager;

        public TrackGenerator(TrackManager manager, TrackGenerationProperties parameters)
        {
            this.manager = manager;
            this.parameters = parameters;
        }

        async internal Task<Track> GenerateTrackAsync(TrackGenerationProperties parameters)
        {
            this.parameters = parameters;

            /*Track generation algorithm:
             * 
             * 1.- Pre-pass getting average sound intensity for each interval. Get also average intensity for the song
             *  -TODO: once this data is processed it should be stored in the Song Scriptable object in order to avoid future passes.
             *  
             * 2.- Pass through intensity vector modifying values if:
             *  -First beat of a bar: add intensity
             *  [-If intensity of an interval is greater than the average] <-- This can be replaced with a sharpening filter
             *  -Being in a greater-intesity song segment like the chorus
             *  
             * 3.- Post process filters (low pass, sharpen, blur...)
             *  
             * 4.- Normalize values of the intensity vector between 0 and 1 and get the average
             * 
             * Intensity vector finished------------------------------------------------------------
             * 
             * 5.- For each interval:             
             *  -Balancing difficulty, homogeneity and playability, populate the track data structure
             */

            float averageIntensity, maxNewIntens = 0f;
            float[] intensities = parameters.seedSong.GetRawIntensitiesVector(parameters.SecondsPerInterval, out averageIntensity);

            AnalyzeIntensities(parameters, ref averageIntensity, ref maxNewIntens, intensities);


            TrackTileParameters[] generatedTrackTiles = GenerateTrackTiles(intensities, averageIntensity);

            //manager.track = new Track(new TrackParameters(intensities, averageIntensity, parameters.intervalTileLength,
            //    generatedTrackTiles, parameters.seedSong), manager);

            return new Track(new TrackParameters(intensities, averageIntensity, parameters.MetersPerInterval,
                generatedTrackTiles, parameters.seedSong), manager);

            //TODO: set a "generating" flag of manager to off

        }

        private void AnalyzeIntensities(TrackGenerationProperties parameters, ref float averageIntensity, ref float maxNewIntens, float[] intensities)
        {
            for (int i = 0; i < intensities.Length; i++)
            {
                if (i % manager.properties.intervalsPerBeat == 0)//If we are in the first beat of a bar
                {
                    intensities[i] += parameters.barInitPush;
                }

                if (intensities[i] > averageIntensity)
                {
                    intensities[i] += parameters.sharpenPush;
                }

                //TODO: song segment increase, to do when song segments are implemented

                if (intensities[i] > maxNewIntens) maxNewIntens = intensities[i];

            }

            //Normalization and new average loop
            averageIntensity = 0f;

            for (int i = 0; i < intensities.Length; i++)
            {
                intensities[i] /= maxNewIntens;
                averageIntensity += intensities[i];
            }

            averageIntensity /= intensities.Length;
        }

        private TrackTileParameters[] GenerateTrackTiles(float[] intensities, float averageIntensity)
        {
            //Danger placement on the track. Normalized intensities allow us to use the numbers as direct randoms

            //TODO: suggestion: respect powerful silences (maybe those that are at less that the 1st quartil)

            int estimatedDangers = Mathf.CeilToInt(intensities.Length * parameters.avgDangersPerSecond / parameters.SecondsPerInterval);

            TrackTileParameters[] tilesData = new TrackTileParameters[intensities.Length];

            float currentDangersPerSecond = parameters.avgDangersPerSecond;//We start at high to have few starting dangers
            //float dangersPerSecond = 0f;
            int lastSecondTilesAmount = Mathf.CeilToInt(parameters.IntervalsPerSecond);
            Queue<TrackTileParameters> lastTiles = new Queue<TrackTileParameters>(lastSecondTilesAmount); //Tiles in the last second
            //TrackTileData tileData = new TrackTileData(-1, null);

            float intervalSeconds = parameters.SecondsPerInterval;//Times dangers placed that is 1

            int debug_totalDangers = 0;
            for (int i = 0; i < tilesData.Length; i++)
            {
                //Debug.Log("DPS: " + dangersPerSecond);
                float generationRndThreshold = parameters.GetGenerationRandomThreshold(currentDangersPerSecond, intensities[i], 1f);
                float randomNum = Random.Range(0f, 1f);

                if (currentDangersPerSecond < parameters.MaxDangersPerSecondAllowed &&
                    (currentDangersPerSecond <= 0f || randomNum < generationRndThreshold))
                {
                    //Debug.Log("Danger Generated");
                    //TODO: proposal: if the rnd.range()-threshold was lesser than .25 (1 quartil), generate more than 1 danger
                    tilesData[i] = new TrackTileParameters(i * intervalSeconds, GenerateTrackDangers());
                    debug_totalDangers++;
                    //dangersPerSecond += intervalSeconds * tilesData[i].startingDangers.Length;
                    currentDangersPerSecond++;
                }
                else
                {
                    tilesData[i] = new TrackTileParameters(i * intervalSeconds, null);
                }

                //dangersPerSecond = Mathf.Max(0f, dangersPerSecond - intervalSeconds);

                //We advance the queue || TODO: Commented because innecesary difficulty as dangers can just be on 1 by 1 basis right now
                lastTiles.Enqueue(tilesData[i]);
                if (lastTiles.Count >= lastSecondTilesAmount)
                {
                    TrackTileParameters leavingTile = lastTiles.Dequeue();

                    TrackDangerParameters[] dangers = leavingTile.startingDangers;

                    //dangersPerSecond -= parameters.SecondsPerInterval * (dangers == null ? 0 : dangers.Length);
                    currentDangersPerSecond -= dangers == null ? 0 : dangers.Length;
                }
                else currentDangersPerSecond = Mathf.Max(0f, currentDangersPerSecond - parameters.SecondsPerInterval);

            }

#if UNITY_EDITOR
            if (manager.debugMessages)
                Debug.Log(debug_totalDangers / parameters.seedSong.GetDuration());
#endif

            return tilesData;
        }

        #region DangerGeneration

        int lastType = -1;
        int sameDangersTogether = 0;

        private TrackDangerParameters[] GenerateTrackDangers()
        {
            //TODO: Allow for more than 1 simultaneous danger && allow for dangers that last more than 1 tile
            return new TrackDangerParameters[1] { CreateDanger() };
        }

        private TrackDangerParameters CreateDanger()
        {
            TrackDanger selectedDanger = parameters.generableDangers.GetRandom();

            if (lastType == (int)selectedDanger.parameters.type)
            {
                if (sameDangersTogether >= parameters.maxSameDangersTogether)
                {
                    selectedDanger = parameters.generableDangers.GetRandomWithExclusion2(selectedDanger);
                }
                else
                {
                    sameDangersTogether++;
                }
            }
            else
            {
                lastType = (int)selectedDanger.parameters.type;
                sameDangersTogether = 0;
            }

            return new TrackDangerParameters(selectedDanger.parameters, 1);
        }
        #endregion
    }
}
