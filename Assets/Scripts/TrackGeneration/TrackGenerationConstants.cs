﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: 
 * -Think about changing time measures from seconds to beats, as it's more natural to the system
 *          One disadvantage is that that metric would be dependant on bpm and this one is not
 *          
 * -make difficulty value count somehow or embed it into all other parameters
*/
using System;
using System.Collections.Generic;
using UnityEngine;
using MusicGeneration;

namespace TrackGeneration
{    
    [Serializable] public class TrackDangerDictionary : SerializableDictionary<TrackDangerType, TrackDanger> { }

    public enum TrackDangerType
    {
        NONE,
        SILENCE,
        LASER_STATIC, LASER_MOBILE, LASER_ROTATING, LASER_DYNAMIC,
        DRONE_STATIC, DRONE_GATEKEEPER, DRONE_MOBILE,
        GAP
    }

}
