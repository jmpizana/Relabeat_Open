﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO:
 * URGENT: implement oclussion-like system for tiles
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MusicGeneration;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PizanaUtility;

namespace TrackGeneration
{
    public class TrackManager : MonoBehaviour
    {

        public static TrackManager instance;

        public TrackGenerationParameters parametersObject;

        [HideInInspector] public TrackGenerationProperties properties;

        TrackGenerator generator;
        public Track track;
        CharacterManager player;
        MusicManager musicManager;
        TrackTile[] tiles;
        Transform trackParent;

        public bool debugMessages = true;

        private void Awake()
        {
            if (instance != null && instance != this) Destroy(gameObject);
            else instance = this;

            properties = parametersObject.properties;
            generator = new TrackGenerator(this, properties);
        }

        public void Initialize()
        {
            player = CharacterManager.instance;
            musicManager = MusicManager.instance;
        }

        async public Task<bool> GenerateDataTrackAsync()
        {
            properties.seedSong = musicManager.Song;
            //StartCoroutine(generator.GenerateTrackAsync(parameters));
            track = await generator.GenerateTrackAsync(properties);

            return track != null;
        }

        /*public void GeneratePhysicalTrackAsync()
        {
          //TODO: 
        }*/

        public void SaveTrack()
        {
            throw new NotImplementedException();
        }

        public void LoadTrack()
        {
            throw new NotImplementedException();
        }

        public void UpdateOcclussion(GameManager manager)
        {
            throw new NotImplementedException();
        }

        async public Task<bool> GeneratePhysicalTrack()
        {
            //Generates the actual track from the trackdata, with this as the parent

            //TODO: take in account dangers that persist more than 1 tile

            if (track == null) throw new UnityException("Trying to generate physical track without track data");

            trackParent = new GameObject("TrackParent").transform;
            trackParent.parent = transform;

            TrackTileParameters[] tilesData = track.parameters.trackTilesData;
            tiles = new TrackTile[tilesData.Length];

            InstantiateInitialVoidTreadmill();

            float time = 0f;

            for (int i = 0; i < tilesData.Length; i++)
            {
                tiles[i] = InstantiateTile(time, tilesData[i], i % (properties.intervalsPerBeat * 4));//TODO: por ahora esto solo sirve para ritmos 4/4, habrá que generalizar
                time += properties.SecondsPerInterval;
            }

            InstantiateFinalTile(time);

            return true;
        }

      
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private TrackTile InstantiateTile(float time, TrackTileParameters data, int orderInBar)
        {
            Vector3 position = GetPositionForSongTime(time);

            TrackTile tile = Instantiate(properties.rootTile, position, Quaternion.identity, trackParent);

            tile.name = time.ToString();
            tile.parameters = data;

            tile.InstantiatePhysical(properties, orderInBar);

            return tile;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Vector3 GetPositionForSongTime(float time)
        {
            //TODO: quítate de la enfermedad de los Singleton, retraksao
            return properties.trackDirection *
                ((properties.MetersPerSecond * (time + GameManager.instance.parameters.properties.secondsBeforeSongStart * 0.5f))
                + properties.MetersPerInterval * 0.5f);
        }

        private void InstantiateInitialVoidTreadmill()
        {
            float length = properties.MetersPerSecond * GameManager.instance.parameters.properties.secondsBeforeSongStart;

            //Vector3 position = length * properties.trackDirection * 0.5f;
            Vector3 position = new Vector3
                (0f,
                -0.5f * properties.tileSectionDimensions.y - properties.initialTreadmillTile.GetComponent<MeshRenderer>().bounds.size.y * 0.5f,
                0f);

            Transform initialTreadmill = Instantiate(properties.initialTreadmillTile, trackParent);

            initialTreadmill.position = position;

            initialTreadmill.localScale = (Quaternion.Inverse(initialTreadmill.rotation) *
                new Vector3(properties.tileSectionDimensions.x, 1f, length)).Abs();
        }

        private void InstantiateFinalTile(float finalTime)
        {
            //float length = properties.MetersPerSecond * GameManager.instance.parameters.properties.secondsBeforeSongStart;
            Vector3 position = GetPositionForSongTime(finalTime);

            Transform finalTile = Instantiate(properties.finalTile, trackParent);
            finalTile.position = position;
            //finalTile.localScale = (Quaternion.Inverse(finalTile.rotation) *
            //    new Vector3(properties.tileSectionDimensions.x, 1f, length)).Abs();
        }

        public void InverseMoveCharacter(Vector3 characterPosition)
        {
            //TODO: take care of LOD, float overflow and other elements, connect to the game

            transform.position = -characterPosition;
            //trackData.MoveTrack(distance);
        }

        private float trackLength = -1f;
        public float TrackLength
        {
            get
            {
                if (trackLength <= 0)
                    trackLength = musicManager.Song.GetDuration() * properties.metersPerBeat * properties.seedSong.GetBPM() / 60f;
                return trackLength;

            }
        }
    }
}
