﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MusicGeneration;
using System;

public static class TrackSongExtension
{
    /// <summary>
    /// Returns the avg audio intensities windowed at each precisionInterval for the total duration of the song in a [0,1] range.
    /// </summary>
    /// <param name="song"></param>
    /// <param name="precision"></param>
    /// <returns></returns>
    /*
    public static float[] GetRawIntensitiesVector(this SongComposed song, float intervalsToSeconds, out float averageIntensity)
    {
        float totalDuration = song.songParameters.duration;
        float timePerInterval = intervalsToSeconds;

        int intervalsAmount = Mathf.CeilToInt(totalDuration / timePerInterval);

        float[] intensities = new float[intervalsAmount];
        averageIntensity = 0;

        for (int i = 0; i < intervalsAmount; i++)
        {
            intensities[i] = song.GetAudioIntensityForInterval(i * timePerInterval, timePerInterval);
            averageIntensity += intensities[i];
        }

        averageIntensity /= intervalsAmount;//TODO: average intensities should be measured on a per-song segment basis, not for the entire song

        return intensities;
    }*/


    /// <summary>
    /// Calculates average audio intensity for all active tracks in a time interval
    /// </summary>
    /// <param name="song"></param>
    /// <param name="time"></param>
    /// <param name="timeInterval"></param>
    /// <returns></returns>
    public static float GetAudioIntensityForInterval(this SongComposed song, float time, float timeInterval)
    {
        int selectedSegmentIndex = Mathf.FloorToInt(song.songParameters.SecondsToSegments * time);
        float innerTime = time % song.songParameters.SegmentsToSeconds;
        SongSegment selectedSegment = song.composedSegments[selectedSegmentIndex].second;

        return selectedSegment.GetAudioIntensityForSegment(innerTime, timeInterval);

        //Comented because we set analysis to not bleed to other segments
        //if(innerTime + timeInterval > song.songParameters.SegmentsToSeconds) //If the sampled end is greater than the segment duration
        //pick the next segment and add it to analysis

    }

    //TODO: A middle layer has to be devised here regarding SongSegments that OFFSETS THE TIME TO THE BEGINNING OF THE CURRENT ONE

    private static float GetAudioIntensityForSegment(this SongSegment segment, float time, float timeInterval)
    {
        float intensity = 0f;

        intensity += GetAudioIntensityForIntervalInLayer(segment.beatSamples, time, timeInterval);
        intensity += GetAudioIntensityForIntervalInLayer(segment.melodySamples, time, timeInterval);
        intensity += GetAudioIntensityForIntervalInLayer(segment.harmonySamples, time, timeInterval);

        if (float.IsNaN(intensity))
            throw new UnityException("Found a NaN intensity");

        return intensity / 3f;
    }

    public static float GetAudioIntensityForIntervalInLayer(List<Sample> samples, float time, float timeInterval)
    {
        if (samples.Count == 0) return 0f;

        float intensity = 0f;

        for (int i = 0; i < samples.Count; i++)
        {
            intensity += samples[i].GetAudioIntensityForIntervalInSample(time, timeInterval);
        }

        return intensity / samples.Count;
    }

    private static float GetAudioIntensityForIntervalInSample(this Sample sample, float time, float timeInterval)
    {
        if (timeInterval > sample.parameters.duration) throw new Exception("Song analysis interval is greater than its duration");

        float intensity = 0;
        AudioClip clip = sample.parameters.clip;
        int dataSamples = Mathf.RoundToInt(clip.frequency * timeInterval);
        float[] data = new float[dataSamples]; //Its length has to correspond to the time interval
        float timeOffset = time % sample.parameters.duration; //Real time offset for the sample
        int offsetSamples = Mathf.RoundToInt(clip.frequency * timeOffset);

        if (!clip.GetData(data, offsetSamples)) throw new Exception(clip.name + " audio data could not be loaded");

        //Samples are got in [-1, +1]
        for (int i = 0; i < dataSamples; i++)
        {
            intensity += data[i];
        }

        //TODO: careful, this could lead to a lot of error in intensity
        return ((intensity / dataSamples) + 1f) * 0.5f;//Intensity in [0,1 range]
    }

    /*public static float GetInstantIntensity(this SongComposed song, float time)
    {
        //TODO: check if this works

        float frequency = song.baseSamples.essential[0].parameters.clip.frequency;
        float intensity = song.GetAudioIntensityForInterval(time, 2f / frequency); //We take two as a safety measure
        return intensity;
    }*/
    /*
    public static float GetInstantIntensityInLayer(this SongComposed song, MusicLayer layer,  float time)
    {
        if (time < 0) return 0f;
        //TODO: check if this works

        //first we assess the correct segment and then its samples
        float frequency = song.baseSamples.essential[0].parameters.clip.frequency;
        int selectedSegmentIndex = Mathf.FloorToInt(song.songParameters.SecondsToSegments * time);
        float innerTime = time % song.songParameters.SegmentsToSeconds;

        if (selectedSegmentIndex >= song.composedSegments.Length) return 0f;

        SongSegment selectedSegment = song.composedSegments[selectedSegmentIndex].second;

        List<Sample> selectedLayerSamples = selectedSegment.beatSamples;

        switch (layer)
        {
            case MusicLayer.BASE:
                selectedLayerSamples = selectedSegment.beatSamples;
                break;

            case MusicLayer.HARMONY:
                selectedLayerSamples = selectedSegment.harmonySamples;
                break;

            case MusicLayer.MELODY:
                selectedLayerSamples = selectedSegment.melodySamples;
                break;

            default:
                selectedLayerSamples = selectedSegment.beatSamples;
                break;
        }
        float intensity = GetAudioIntensityForIntervalInLayer(selectedLayerSamples, time, 2f / frequency); //We take two as a safety measure

        return intensity;
    }*/
}
