﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrackGeneration;

[CreateAssetMenu( menuName = "Parameters/SoundEffectsParameters", order = 1), System.Serializable]
public class SoundEffectsParameters : ScriptableObject {

    public int maxElementsPerPool;

    public List<DangerTypeClipPair> dangerClips;

}

[System.Serializable]
public struct DangerTypeClipPair
{
    public TrackDangerType dangerType;
    public AudioClip[] clips;

    public DangerTypeClipPair(TrackDangerType dangerType, AudioClip[] clips)
    {
        this.dangerType = dangerType;
        this.clips = clips;
    }
}

[System.Serializable]
public class ClipsDangerDictionary: SerializableDictionary<TrackDangerType, AudioClip> { }