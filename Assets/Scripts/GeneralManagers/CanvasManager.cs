﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * Manages canvas AND visual effects
 * 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;


public class CanvasManager : MonoBehaviour
{
    public static CanvasManager instance;
    public Canvas worldCanvas;
    public CanvasParameters parameters;

    private void Awake()
    {
        if (instance != this && instance != null) Destroy(gameObject);
        else instance = this;
    }

    public void Initialize(CharacterManager charManager)
    {
        //int energyTicks = charManager.parameters.maxEnergy;
        InitializeEnergyBar(charManager.parameters.maxEnergy);
        InitializeHealthbar(charManager.parameters.maxHealth);

        parameters.pauseGamePanel.updateMode = AnimatorUpdateMode.UnscaledTime;
        parameters.winGamePanel.updateMode = AnimatorUpdateMode.UnscaledTime;
        parameters.loseGamePanel.updateMode = AnimatorUpdateMode.UnscaledTime;
    }

    private void InitializeHealthbar(int maxHealth)
    {
        parameters.healthTicks = new Image[maxHealth];

        for (int i = 0; i < maxHealth; i++)
        {
            parameters.healthTicks[i] = Instantiate(parameters.healthTickPrefab, parameters.healthBarPanel);
        }
    }

    private void InitializeEnergyBar(float energyPoints)
    {
        parameters.energyTicks = new Image[Mathf.CeilToInt(energyPoints)];

        for (int i = 0; i < energyPoints; i++)
        {
            parameters.energyTicks[i] = Instantiate(parameters.energyTickPrefab, parameters.energyBarPanel);
        }
    }

    public void BlackFade(bool isFadeIn)
    {
        BlackFade(isFadeIn, parameters.defaultFadeBlackTime, parameters.defaultFadeBlackSteps);
    }

    async public void BlackFade(bool isFadeIn, float time, int steps)
    {
        float timeJump = time / (float)steps;
        WaitForSeconds waitTime = new WaitForSeconds(timeJump);

        Color initialColor = isFadeIn ? Color.clear : Color.black,
            finalColor = isFadeIn ? Color.black : Color.clear;

        for (float i = 0; i < steps; i++)
        {
            parameters.blackPanel.color = Color.Lerp(initialColor, finalColor, i / (steps - 1));
            await waitTime;
        }
    }

    public void UpdateCanvas(CharacterManager charManager)
    {
        SetEnergyBarValue(charManager.energy);
        SetHealthBarValue(charManager.health);
    }

    int previousTick = -1;
    float inTickValue;
    private void SetEnergyBarValue(float energyValue)
    {
        int energyPoints = parameters.energyTicks.Length;

        //if (energyValue < 0 || energyValue > energyPoints) return;

        int tick = Mathf.FloorToInt(energyValue);
        if (tick == energyPoints)
        {
            tick--;
            inTickValue = 1f;
        }
        else inTickValue = energyValue % 1;

        if (previousTick >= 0 && previousTick != tick)
        {
            if (previousTick < tick)
                parameters.energyTicks[previousTick].fillAmount = 1f;
            else
                parameters.energyTicks[previousTick].fillAmount = 0f;
        }

        parameters.energyTicks[tick].fillAmount = inTickValue;

        previousTick = tick;
    }

    int previousHealth = -1;
    private void SetHealthBarValue(int currentHealth)
    {
        if (currentHealth < 0) return;
        if (previousHealth >= 0 && currentHealth != previousHealth)
        {
            if (currentHealth > previousHealth)
            {
                parameters.healthTicks[currentHealth - 1].sprite = parameters.filledHealthTick;
            }
            else
                parameters.healthTicks[currentHealth].sprite = parameters.emptyHealthTick;
        }

        previousHealth = currentHealth;
    }

    public void SetLoseGamePanel(bool open)
    {
        if (open)
            parameters.loseGamePanel.SetTrigger("Open");
        else
            parameters.loseGamePanel.SetTrigger("Close");
    }

    public void SetWinGamePanel(bool open)
    {
        if (open)
            parameters.winGamePanel.SetTrigger("Open");
        else
            parameters.winGamePanel.SetTrigger("Close");
    }

    public void SetPauseGamePanel(bool open)
    {
        if (open)
            parameters.pauseGamePanel.SetTrigger("Open");
        else
            parameters.pauseGamePanel.SetTrigger("Close");
    }

    [System.Serializable]
    public struct CanvasParameters
    {
        [Header("Health Bar")]
        public RectTransform healthBarPanel;
        public Image healthTickPrefab;
        public Sprite filledHealthTick, emptyHealthTick;
        public Image[] healthTicks;

        [Header("Energy Bar")]
        public RectTransform energyBarPanel;
        public Image energyTickPrefab;
        public Image[] energyTicks;

        [Header("Black Fade")]
        public float defaultFadeBlackTime;
        public int defaultFadeBlackSteps;
        public Image blackPanel;

        [Header("GamePanels")]
        public Animator loseGamePanel;
        public Animator winGamePanel;
        public Animator pauseGamePanel;
    }

}
