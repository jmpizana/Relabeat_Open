﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrackGeneration;
using System;
using Random = UnityEngine.Random;
using System.Runtime.CompilerServices;
using PizanaUtility;

public class EnvironmentManager : MonoBehaviour
{
    public static EnvironmentManager instance;

    public float buildingsSeparationFromTrack = 3f,
        buildingsAverageInternalSeparation = 2f, buildingsInternalSeparationVariance = 1f,
        rowsSeparation = 15f,
        backBuildingGenerationBuffer = 10f, frontBuildingGenerationBuffer = 30f,
        buildingsFloor = -20f,
        defaultTrackLength = 400f
        ;

    public int rowsDepth = 3;

    public MeshRenderer[] buildings;

    public List<BuildingRow> buildingRows;

    Transform buildingsParent;
    TrackManager trackManager;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;
    }

    public void GenerateBuildings(TrackManager trackManager)
    {
        this.trackManager = trackManager;

        buildingsParent = new GameObject("BuildingsParent").transform;
        buildingsParent.parent = transform;

         buildingRows = new List<BuildingRow>(2 * rowsDepth);
        buildingRows.AddRange(GenerateRows(Vector3.left));
        buildingRows.AddRange(GenerateRows(Vector3.right));
    }

    public void GenerateBuildings()
    {
        buildingsParent = new GameObject("BuildingsParent").transform;
        buildingsParent.parent = transform;

        buildingRows = new List<BuildingRow>(2 * rowsDepth +1);
        buildingRows.Add(GenerateRow(Vector3.forward));
        buildingRows.AddRange(GenerateRows(Vector3.left));
        buildingRows.AddRange(GenerateRows(Vector3.right));
    }

    private List<BuildingRow> GenerateRows(Vector3 direction)
    {
        List<BuildingRow> sideRows = new List<BuildingRow>(rowsDepth);
        float accumulatedDistance = buildingsSeparationFromTrack;
        for (int i = 0; i < rowsDepth; i++)
        {
            sideRows.Add(GenerateRow(direction.normalized * accumulatedDistance + Vector3.back * backBuildingGenerationBuffer));
            accumulatedDistance += sideRows[i].width + GetRandomBuildingGap();
        }

        return sideRows;
    }

    private BuildingRow GenerateRow(Vector3 startingPosition)
    {
        List<MeshRenderer> rowBuildings = new List<MeshRenderer>();
        float currentLength = 0f;
        float maxWidth = 0f;
        for (float j = 0; j < RowMinLength;)
        {
            MeshRenderer instantiatedBuilding = InstantiateBuilding(startingPosition + Vector3.forward * j);
            rowBuildings.Add(instantiatedBuilding);
            currentLength = instantiatedBuilding.bounds.size.z;
            maxWidth = Mathf.Max(maxWidth, instantiatedBuilding.bounds.size.x);
            j += currentLength + GetRandomBuildingGap();
        }

        return new BuildingRow(rowBuildings, maxWidth);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private float GetRandomBuildingGap()
    {
        return buildingsAverageInternalSeparation + Random.Range(-1f, 1f) * buildingsInternalSeparationVariance;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]

    private MeshRenderer InstantiateBuilding(Vector3 position)
    {
        MeshRenderer buildingToInstantiate = buildings.GetRandom();

        MeshRenderer instantiatedBuilding = Instantiate<MeshRenderer>(buildingToInstantiate, 
            position + Vector3.up * (buildingsFloor + buildingToInstantiate.bounds.extents.y),
            GetRandomYRotation(), buildingsParent);

        return instantiatedBuilding;
    }

    public float rowMinLength = -1f;
    float RowMinLength
    {
        get
        {
            if (rowMinLength <= 0)
            {
                if(trackManager != null)
                    rowMinLength = trackManager.TrackLength + frontBuildingGenerationBuffer + backBuildingGenerationBuffer;
                else
                    rowMinLength = defaultTrackLength + frontBuildingGenerationBuffer + backBuildingGenerationBuffer;
            }
            return rowMinLength;
        }
    }

    private Quaternion GetRandomYRotation()
    {
        //TODO: por ahora es libre, se puede clampear a multiplos de 90, 45...

        return Quaternion.Euler(0, Random.Range(0f, 360f), 0);
    }

    [Serializable]
    public struct BuildingRow
    {
        public List<MeshRenderer> buildings;
        public float width;

        public BuildingRow(List<MeshRenderer> buildings, float width)
        {
            this.buildings = buildings;
            this.width = width;
        }
    }
}
