﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;
using TrackGeneration;

public class SoundEffectsManager : MonoBehaviour
{
    public static SoundEffectsManager instance;

    public SoundEffectsParameters parameters;
    public Dictionary<AudioClip, SourcesPlayer> sourcesDictionary;
    public Dictionary<TrackDangerType, AudioClip[]> dangerClipsDictionary;

    private Transform sourcesParent;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;
    }

    public void Initialize()
    {
        sourcesParent = new GameObject("Sound FX Parent").transform;
        sourcesParent.parent = transform;

        List<DangerTypeClipPair> dangerClips = parameters.dangerClips;
        dangerClipsDictionary = new Dictionary<TrackDangerType, AudioClip[]>(dangerClips.Count);
        for (int i = 0; i < dangerClips.Count; i++)
        {
            dangerClipsDictionary[dangerClips[i].dangerType] = dangerClips[i].clips;
        }

        sourcesDictionary = new Dictionary<AudioClip, SourcesPlayer>();
    }

    public void PlaySound(AudioClip clip, bool loop = false, float pitch = 1)
    {
        SourcesPlayer sp;

        if (!sourcesDictionary.TryGetValue(clip, out sp))
        {
            sp = new SourcesPlayer(clip, parameters.maxElementsPerPool, sourcesParent);
            sourcesDictionary[clip] = sp;
            sp.Play(loop, pitch);
        }
        else
        {
            sp.Play(loop, pitch);
        }
    }


	public class SourcesPlayer
    {
        public int Capacity
        {
            get { return sources.Length; }
        }

        //public int SourcesPlaying { get { return sources.Length - availableSources.Count; } }

        public int playing = 0;
        public int initializedSources = 0;

        AudioClip clip;
        AudioSource[] sources;
        Queue<AudioSource> availableSources;

        Transform sourcesParent;
        public SourcesPlayer(AudioClip clip, int capacity, Transform sourcesParent = null)
        {
            this.clip = clip;
            sources = new AudioSource[capacity];
            availableSources = new Queue<AudioSource>(capacity);
            this.sourcesParent = sourcesParent;
        }

        public void Play(bool loop = false, float pitch = 1)
        {
            if (playing < Capacity)
            {
                if(initializedSources < sources.Length)//If not all available source slots have been filled
                {
                    AudioSource s = new GameObject(clip.name + "Source").AddComponent<AudioSource>();

                    s.transform.parent = sourcesParent;
                    s.clip = clip;
                    sources[initializedSources] = s;
                    initializedSources++;

                    PlaySource(loop, pitch, s);
                }
                else
                {
                    //If all available slots have been filled
                    PlaySource(loop, pitch, availableSources.Dequeue());
                }
               
            }
            //If all sources are playing the next sound is not played
        }

        private async void PlaySource(bool loop, float pitch, AudioSource s)
        {
            playing++;
            s.Play();
            s.pitch = pitch;
            s.loop = loop;

            if (!loop)
            {
                await new WaitForSeconds(clip.length);
                playing--;
                s.Stop();
                availableSources.Enqueue(s);
            }
        }

        public void StopAll()
        {
            availableSources.Clear();
            for (int i = 0; i < initializedSources; i++)
            {
                sources[initializedSources].Stop();
            }
            playing = 0;

        }
    }
}   

