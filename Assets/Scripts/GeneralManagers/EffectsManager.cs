﻿//TODO: camera shake

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;
using MusicGeneration;

public class EffectsManager : MonoBehaviour
{
    public static EffectsManager instance;

    public EffectsParameters parameters;

    Transform profilesVolumesParent;
    PostProcessVolume normalVolume, damagedVolume, blackScreenVolume;
    Bloom bloom;
    ChromaticAberration chrAberr;
    LensDistortion distortion;
    float melodyIntens, baseIntens, bloomValue, chrAbrValue, distortValue;
    MusicManager musicManager;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;
    }

    public void Initialize(CharacterManager charManager, MusicManager musicManager)
    {
        this.musicManager = musicManager;

        profilesVolumesParent = new GameObject("Profiles Volumes Parent").transform;
        profilesVolumesParent.parent = transform;
        profilesVolumesParent.gameObject.layer = charManager.gameObject.layer;

        normalVolume = CreatePostProVolume(charManager, parameters.normalProfile, 1f, 1f);
        damagedVolume = CreatePostProVolume(charManager, parameters.damagedProfile, 1f, 0f);
        blackScreenVolume = CreatePostProVolume(charManager, parameters.blackScreenProfile, 1f, 1f);

        if (!normalVolume.profile.TryGetSettings(out bloom)) throw new UnityException("Normal Game profile does not have a Bloom setting");
        if (!normalVolume.profile.TryGetSettings(out chrAberr)) throw new UnityException("Normal Game profile does not have a Chromatic Aberration setting");
        if (!normalVolume.profile.TryGetSettings(out distortion)) throw new UnityException("Normal Game profile does not have a Lens Distortion setting");

        parameters.bloomVariator.Initialize(bloom.intensity);
        parameters.chrAbrVariator.Initialize(chrAberr.intensity);
        parameters.distortVariator.Initialize(distortion.intensity);
    }

    private void Update()
    {
        SoundVisualFX();
    }

    private void SoundVisualFX()
    {
        if (!parameters.doSoundVisualFX || !musicManager.Playing) return;
        melodyIntens = musicManager.Song.GetInstantIntensityInLayer(MusicLayer.MELODY, musicManager.playbackTime);
        baseIntens = musicManager.Song.GetInstantIntensityInLayer(MusicLayer.BASE, musicManager.playbackTime);

        bloomValue = parameters.bloomVariator.Update(bloom.intensity, baseIntens, Time.deltaTime);
        chrAbrValue = parameters.chrAbrVariator.Update(chrAberr.intensity, baseIntens, Time.deltaTime);
        distortValue = parameters.distortVariator.Update(distortion.intensity, baseIntens, Time.deltaTime);

        bloom.intensity.Override(bloomValue);
        chrAberr.intensity.Override(chrAbrValue);
        distortion.intensity.Override(distortValue);

        //Debug.Log(distortion.intensity.value);
    }

    public void SetSoundDrivenActive(bool active)
    {
        parameters.bloomVariator.active = active;
        parameters.chrAbrVariator.active = active;
        parameters.distortVariator.active = active;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private PostProcessVolume CreatePostProVolume(CharacterManager charManager, PostProcessProfile profile, float priority, float initialWeight)
    {
        PostProcessVolume volume = PostProcessManager.instance.QuickVolume(charManager.gameObject.layer, priority, profile.settings.ToArray());
        volume.weight = initialWeight;
        volume.isGlobal = true;
        //if (initialWeight == 0) volume.enabled = false;

        volume.gameObject.hideFlags = volume.gameObject.hideFlags & ~(HideFlags.HideInInspector | HideFlags.HideInHierarchy | HideFlags.NotEditable);
        return volume;
    }

    public void BlackFade(bool isFadeIn)
    {
        BlackFade(isFadeIn, parameters.defaultFadeBlackTime, null);
    }

    public void BlackFade(bool isFadeIn, Action callback)
    {
        BlackFade(isFadeIn, parameters.defaultFadeBlackTime, callback
            //, parameters.defaultFadeBlackSteps
            );
    }

    private void BlackFade(bool isFadeIn, float time, Action callback
        //, int steps
        )
    {

        float targetWeight = isFadeIn ? 1f : 0f;
        blackScreenVolume.enabled = true;

        DOTween.To((float x) => blackScreenVolume.weight = x, 1f - targetWeight, targetWeight, time)
            .SetEase(parameters.blackFadeEase)
            //.SetUpdate(true)
            .OnComplete(() =>
           {
               if (!isFadeIn) blackScreenVolume.enabled = false;
               //if (isFadeIn) Debug.Log("Fade to black");

               callback?.Invoke();
           });

    }

    public void DamagedEffect(float time)
    {
        //damagedVolume.enabled = true;

        CameraShake();

        DOTween.To((float x) => damagedVolume.weight = x, 0f, 1f, time * parameters.damagedTimeMultiplier)
            .SetEase(parameters.damagedEase)
            //.SetUpdate(true)
            .OnComplete(() =>
                damagedVolume.weight = 0f
                //damagedVolume.enabled = false
            );
    }

    public void CameraShake()
    {
        //Debug.Log("Camera Shaking");
        Camera camera = Camera.main;
        camera.transform.DOKill(true);
        camera.transform.DOShakePosition(parameters.shakeDuration, parameters.shakeIntensity, parameters.shakeAmount);
    }

    public void DoCameraDistort()
    {
        DoCameraDistort(parameters.blackHoleDistortEndValue, parameters.blackHoleDistortDuration, null);
    }

    public void DoCameraDistort(Action callback)
    {
        DoCameraDistort(parameters.blackHoleDistortEndValue, parameters.blackHoleDistortDuration, callback);
    }

    public void DoCameraDistort(float endValue, float duration, Action callback)
    {
        parameters.distortVariator.active = false;

        //float intens = distortion.intensity;
        DOTween.To(() => distortion.intensity, (float x) => distortion.intensity.Override(x), endValue, duration)
            .OnComplete(() =>
            {
                distortion.intensity.Override(endValue);
                callback?.Invoke();
            });
        //distortion.intensity
    }

    public void DisposeEffects()
    {
        OnDestroy();

        Destroy(normalVolume);
        Destroy(damagedVolume);
        Destroy(blackScreenVolume);
    }

    public void OnDestroy()
    {
        bloom.intensity.Override(parameters.bloomVariator.InitialValue);
        chrAberr.intensity.Override(parameters.chrAbrVariator.InitialValue);
        distortion.intensity.Override(parameters.distortVariator.InitialValue);
    }

    [System.Serializable]
    public struct EffectsParameters
    {
        [Header("Camera Shake")]
        public float shakeIntensity;
        public float shakeDuration;
        public int shakeAmount;

        [Header("Black Fade")]
        public float defaultFadeBlackTime;
        public int defaultFadeBlackSteps;
        public Ease blackFadeEase;
        //public Image blackPanel;

        [Header("Damaged Effects"), Tooltip("How much time proportional to invulnerability should the damage effects apply.")]
        public float damagedTimeMultiplier;
        public AnimationCurve damagedEase;
        //public Ease damagedEase;

        [Header("Black Hole Distort Effect")]
        public float blackHoleDistortEndValue;
        public float blackHoleDistortDuration;

        [Header("Post Processing Profiles")]
        public PostProcessProfile normalProfile;
        public PostProcessProfile damagedProfile;
        public PostProcessProfile blackScreenProfile;

        [Header("Sound Driven Values")]
        public bool doSoundVisualFX;
        public SoundDrivenValueDampened bloomVariator;
        public SoundDrivenValueDampened chrAbrVariator;
        public SoundDrivenValueDampened distortVariator;
    }

}
