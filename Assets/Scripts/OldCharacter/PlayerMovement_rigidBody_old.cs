//using System;
//using UnityEngine;
//using UnityStandardAssets.Characters.FirstPerson;
//using UnityStandardAssets.CrossPlatformInput;

//[RequireComponent(typeof(Rigidbody))]
//[RequireComponent(typeof(CapsuleCollider))]
//public class PlayerMovement_rigidBody_old : MonoBehaviour
//{
//    public static PlayerMovement_rigidBody_old instance;

//    [Serializable]
//    public class MovementSettings
//    {

//        public float ForwardSpeed = 8.0f;   // Speed when walking forward
//        public float BackwardSpeed = 4.0f;  // Speed when walking backwards
//        public float StrafeSpeed = 4.0f;    // Speed when walking sideways
//        public float acceleration = 8f;
//        //public KeyCode RunKey = KeyCode.LeftShift;
//        public float groundJumpForce = 30f, wallJumpForce = 60f;
//        public AnimationCurve SlopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));
//        [HideInInspector] public float CurrentTargetSpeed = 8f;

//        public void UpdateDesiredTargetSpeed(Vector2 input)
//        {
//            if (input == Vector2.zero) return;
//            if (input.x > 0 || input.x < 0)
//            {
//                //strafe
//                CurrentTargetSpeed = StrafeSpeed;
//            }
//            if (input.y < 0)
//            {
//                //backwards
//                CurrentTargetSpeed = BackwardSpeed;
//            }
//            if (input.y > 0)
//            {
//                //forwards
//                //handled last as if strafing and moving forward at the same time forwards speed should take precedence
//                CurrentTargetSpeed = ForwardSpeed;
//            }

//        }
//    }


//    [Serializable]
//    public class AdvancedSettings
//    {
//        public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
//        public float groundDrag = 1f;
//        public float stickToGroundHelperDistance = 0.5f; // stops the character
//        //public float slowDownRate = 20f; // rate at which the controller comes to a stop when there is no input
//        public float airControlEasiness = 0.5f;
//        [Tooltip("set it to 0.1 or more if you get stuck in wall")]
//        public float shellOffset; //reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)
//        [Range(0f,1f)]public float jumpingGravityMultiplier = 0.8f;
//    }


//    public Camera cam;
//    public MovementSettings movementSettings = new MovementSettings();
//    public MouseLook mouseLook = new MouseLook();
//    public AdvancedSettings advancedSettings = new AdvancedSettings();

//    [HideInInspector]
//    public Rigidbody rB;
//    public CapsuleCollider capsule;
//    private float m_YRotation;
//    private Vector3 groundContactNormal;
//    private bool jump, previouslyGrounded, jumping, isGrounded;
//    private InputManager inputManager;
//    [HideInInspector]public WallRunHelper wallRunHelper;

//    public Vector3 Velocity
//    {
//        get { return rB.velocity; }
//    }

//    public bool Grounded
//    {
//        get { return isGrounded; }
//    }

//    public bool Jumping
//    {
//        get { return jumping; }
//    }

//    private void Awake()
//    {
//        if (instance != null && instance != this) Destroy(this);
//        instance = this;

//        rB = GetComponent<Rigidbody>();
//        capsule = GetComponent<CapsuleCollider>();
//        wallRunHelper = GetComponentInChildren<WallRunHelper>();
//        //wallRunHelper.parent = this;
//    }
//    private void Start()
//    {
//        mouseLook.Init(transform, cam.transform);
//        inputManager = InputManager.instance;
//    }

//    private void Update()
//    {
//        RotateView();

//        GroundCheck();
//        if (!isGrounded) wallRunHelper.WallRunCheck();
//        else wallRunHelper.SetWallMovement(false);

//        Vector2 movement = inputManager.movement;
//        movementSettings.UpdateDesiredTargetSpeed(movement);
//        jump = inputManager.jump;
        
//        ControlledMovement(movement);
//        JumpControls(movement);
//        Wallrun();

//    }

//    private void Wallrun()
//    {
//        if (wallRunHelper.IsWallRunning())
//        {
//            float tiltAngle = (wallRunHelper.wall2Right ? 1f : -1f) * wallRunHelper.wallRunTiltAngle;

//            mouseLook.wallRunTilt = Mathf.Lerp(mouseLook.wallRunTilt, tiltAngle, wallRunHelper.wallRunTiltSpeed);
//        }
//        else
//        {
//            mouseLook.wallRunTilt = Mathf.Lerp(mouseLook.wallRunTilt, 0, wallRunHelper.wallRunTiltSpeed);
//        }
//    }

//    private void JumpControls(Vector2 movement)
//    {
//        GravityAdjustments();

//        //Si est� en el suelo
//        if (isGrounded)
//        {
//            rB.drag = advancedSettings.groundDrag;
//            rB.useGravity = true;

//            if (jump)
//            {
//                rB.drag = 0f;
//                rB.velocity = new Vector3(rB.velocity.x, 0f, rB.velocity.z);
//                rB.AddForce(new Vector3(0f, movementSettings.groundJumpForce, 0f), ForceMode.Impulse);
//                jumping = true;
//            }

//            if (!jumping && Mathf.Abs(movement.x) < float.Epsilon && Mathf.Abs(movement.y) < float.Epsilon && rB.velocity.magnitude < 1f)
//            {
//                rB.Sleep();
//            }
//        }
//        else if (wallRunHelper.IsWallRunning())
//        {
//            rB.drag = advancedSettings.groundDrag;

//            if (jump)
//            {
//                //Debug.Log("Jump");
//                rB.drag = 0f;
//                rB.velocity = new Vector3(rB.velocity.x, 0f, rB.velocity.z);

//                Vector3 jumpDirection = Vector3.up + wallRunHelper.wallHitInfo.normal;

//                rB.AddForce(jumpDirection * movementSettings.wallJumpForce, ForceMode.Impulse);
//                jumping = true;
//            }
//        }
//        else //Jumping
//        {
//            rB.useGravity = true;
//            rB.drag = 0f;
//            if (previouslyGrounded && !jumping)
//            {
//                StickToGroundHelper();
//            }
//        }

//        inputManager.jump = false;

//    }

//    private void GravityAdjustments()
//    {
//        if (inputManager.jumping && rB.velocity.y >= 0)
//        {
//            Physics.gravity = PhysicsManager.instance.originalGravity * advancedSettings.jumpingGravityMultiplier;
//        }
//        else
//        {
//            Physics.gravity = PhysicsManager.instance.originalGravity;
//        }
//    }

//    private void ControlledMovement(Vector2 movement)
//    {
//        //Fuerza de movimiento voluntaria
//        if ((Mathf.Abs(movement.x) > float.Epsilon || Mathf.Abs(movement.y) > float.Epsilon))
//        {
//            //Por ahora se mueve en la direcci�n de la c�mara. Esto est� mal, el movimiento deber�a ser acumulativo
//            Vector3 desiredMove = cam.transform.forward * movement.y + cam.transform.right * movement.x;
//            desiredMove = Vector3.ProjectOnPlane(desiredMove, groundContactNormal).normalized;

//            desiredMove *= movementSettings.acceleration;

//            //If we have not reached our target speed, we accelerate
//            if (rB.velocity.magnitude < movementSettings.CurrentTargetSpeed)
//            {
//                rB.AddForce(desiredMove * SlopeMultiplier(), ForceMode.Impulse);
//                //rB.velocity += desiredMove * SlopeMultiplier() / rB.mass;

//            }
//            else if (!isGrounded && !wallRunHelper.IsWallRunning()) //If the character is airborne
//            {
//                Vector3 newVel = rB.velocity;
//                float prevY = newVel.y;
//                newVel.y = 0;
//                newVel += advancedSettings.airControlEasiness * desiredMove * SlopeMultiplier() / rB.mass;

//                newVel = newVel.normalized * movementSettings.CurrentTargetSpeed;

//                rB.velocity = new Vector3(newVel.x, prevY, newVel.z);
//            }


//        }
//    }

//    private float SlopeMultiplier()
//    {
//        float angle = Vector3.Angle(groundContactNormal, Vector3.up);
//        return movementSettings.SlopeCurveModifier.Evaluate(angle);
//    }


//    private void StickToGroundHelper()
//    {
//        RaycastHit hitInfo;
//        if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
//                               ((capsule.height / 2f) - capsule.radius) + advancedSettings.stickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
//        {
//            if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
//            {
//                rB.velocity = Vector3.ProjectOnPlane(rB.velocity, hitInfo.normal);
//            }
//        }
//    }

//    private void RotateView()
//    {
//        //avoids the mouse looking if the game is effectively paused
//        if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

//        // get the rotation before it's changed
//        float oldYRotation = transform.eulerAngles.y;

//        mouseLook.LookRotation(transform, cam.transform);

//        if (isGrounded /*|| advancedSettings.airControl*/)
//        {
//            // Rotate the rigidbody velocity to match the new direction that the character is looking
//            Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
//            rB.velocity = velRotation * rB.velocity;
//        }
//    }

//    /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
//    private void GroundCheck()
//    {
//        previouslyGrounded = isGrounded;
//        RaycastHit hitInfo;
//        if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
//                               ((capsule.height / 2f) - capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
//        {
//            isGrounded = true;
//            groundContactNormal = hitInfo.normal;
//        }
//        else
//        {
//            isGrounded = false;
//            groundContactNormal = Vector3.up;
//        }
//        if (!previouslyGrounded && isGrounded && jumping)
//        {
//            jumping = false;
//        }
//    }
//}

