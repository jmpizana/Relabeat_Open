﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(SphereCollider))]
public class GNade : MonoBehaviour
{
    [HideInInspector]
    public GNadeData gNadeData;
    public GNadeArea gNadeAreaPrefab;

    SphereCollider coll;

    private void Awake()
    {
        coll = GetComponent<SphereCollider>();
    }

    private void OnTriggerEnter (Collider collision)
    {
        //We have to make the GNade explode, maybe instantiate some particles
        SetAreaOfEffect();
        Destroy(gameObject);
    }

    void SetAreaOfEffect()
    {
        GNadeArea area = Instantiate<GNadeArea>(gNadeAreaPrefab, transform.position, Quaternion.identity);
        area.gNadeData = gNadeData;
    }
}

[System.Serializable]
public struct GNadeData
{
    public Vector3[] path;
    public Vector3 estimatedStartSpeed;
    public float travelTime, areaDuration, areaRadius, areaGMultiplier;

    /// Physics driven GNade
    public GNadeData(float travelTime, float areaDuration, float areaRadius, float areaGMultiplier, Vector3 estimatedStartSpeed) : this()
    {
        this.travelTime = travelTime;
        this.areaDuration = areaDuration;
        this.areaRadius = areaRadius;
        this.areaGMultiplier = areaGMultiplier;
        this.estimatedStartSpeed = estimatedStartSpeed;
    }

}
