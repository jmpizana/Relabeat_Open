﻿
/*Ideas
 * -Representar no sólo el arco de la granada, sino el rango efectivo en el pto de impacto
 * */
 /*
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GNadeSkill : Skill
{

    [Header("GNade attributes")]
    public GNade gNadePrefab;
    public float maxDistance = 100f, arcHeight = 50f, maxTravelTime = 2f,
        areaDuration = 2f, areaRadius = 5f,
        areaGMultiplier = -1f; //-1 means no gravity, -2 means negative g, etc.

    public int arcSteps = 10;

    float gNadeMass;
    GNadeData lastValidPotentialGNadeData;

    public override void Setup(SkillManager context)
    {
        context.gNadeArc.gameObject.SetActive(false);
        context.gNadeArc = null;
        context.gNadeAreaMarker.transform.localScale = Vector3.one * areaRadius;
        context.gNadeAreaMarker.SetActive(false);
        
        gNadeMass = gNadePrefab.GetComponent<Rigidbody>().mass;
    }

    public override void Update(SkillManager context)
    {
        if (!enabled) return;

        if (InputManager.instance.GNadeAim)
        {
            RaycastHit hit;
            Vector3 destiny;
            Transform user = context.transform,
                camera = context.charCamera.transform;

            //We check the throw destiny
            if (Physics.Raycast(user.position, camera.forward, out hit, maxDistance))
                destiny = hit.point;
            else destiny = camera.position + camera.forward * maxDistance;


            GNadeVisualGuide(context, user.position, destiny, out lastValidPotentialGNadeData);
        }

        if (InputManager.instance.GNadeShoot)
        {
            //ShootGNadeWithPath(context, lastValidPotentialGNadeData);
            ShootPhysicsGNade(context, lastValidPotentialGNadeData);

            SetOffVisualGuide(context);

        }
    }

    private void SetOffVisualGuide(SkillManager context)
    {

        context.gNadeAreaMarker.SetActive(false);

    }
    private void GNadeVisualGuide(SkillManager context, Vector3 origin, Vector3 destiny, out GNadeData gNadeData)
    {

        gNadeData = PredictGNadeThrowValues(origin, destiny);
        context.gNadeAreaMarker.SetActive(true);
        context.gNadeAreaMarker.transform.position = destiny;


    }

    private void ShootPhysicsGNade(SkillManager context, GNadeData gNadeData)
    {
        GNade gNade = GameObject.Instantiate<GNade>(gNadePrefab, context.transform.position, Quaternion.identity);
        gNade.gNadeData = gNadeData;

        Rigidbody rB = gNade.GetComponent<Rigidbody>();

        rB.velocity = gNadeData.estimatedStartSpeed;
    }

    /// <summary>
    /// Predicts necessary values in order for the GNade to be thrown at a given point
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="destiny"></param>
    /// <returns></returns>
    private GNadeData PredictGNadeThrowValues(Vector3 origin, Vector3 destiny)
    {
        Vector3 distance = destiny - origin;

        float totalTravelTime = (distance.magnitude / maxDistance) * maxTravelTime;
        float startSpeedY = (destiny.y - origin.y) / totalTravelTime - 0.5f * Physics.gravity.y * totalTravelTime * gNadeMass;

        //We suppose acceleration is only happening on the y axis
        Vector3 estimatedInitialSpeeds = new Vector3(
          distance.x / totalTravelTime,
          startSpeedY,
          distance.z / totalTravelTime
          );

        return new GNadeData(totalTravelTime, areaDuration, areaRadius, areaGMultiplier, estimatedInitialSpeeds);
    }


    /// <summary>
    ///Does the same as PredictGNadeThrowValues generating also a path in order to be visualized
    ///It stays as an independent func from its sibling
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="destiny"></param>
    /// <returns></returns>
    private GNadeData PredictPhysicsArc(Vector3 origin, Vector3 destiny)
    {
        Vector3[] result = new Vector3[arcSteps];

        Vector3 distance = destiny - origin;

        float totalTravelTime = (distance.magnitude / maxDistance) * maxTravelTime;
        float startSpeedY = (destiny.y - origin.y) / totalTravelTime - 0.5f * Physics.gravity.y * totalTravelTime * gNadeMass;

        //For each iteration we check the y position given fixed speeds on x and z
        for (int i = 0; i < arcSteps; i++)
        {
            float travelAmount = (float)i / ((float)arcSteps - 1f);
            float hypT = travelAmount * totalTravelTime;
            Vector3 hypPosHorizontal = origin + distance * travelAmount;

#if UNITY_EDITOR
            if (hypT > totalTravelTime) Debug.LogWarning("CAUTION: GNade arc point further than destiny");
#endif

            result[i] = new Vector3(
                hypPosHorizontal.x,
                origin.y + startSpeedY * hypT + Physics.gravity.y * hypT * hypT * 0.5f,
                hypPosHorizontal.z
                );
        }

        //We suppose acceleration is only happening on the y axis
        Vector3 estimatedInitialSpeeds = new Vector3(
          distance.x / totalTravelTime,
          startSpeedY,
          distance.z / totalTravelTime
          );

        return new GNadeData(totalTravelTime, areaDuration, areaRadius, areaGMultiplier, estimatedInitialSpeeds);
    }


}
*/