using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;
using UnityStandardAssets.Characters.FirstPerson;


[RequireComponent(typeof(UnityEngine.CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class PlayerMovement : MonoBehaviour
{
    #region Attributes
    public static PlayerMovement instance;

    //public CapsuleCollider capsule;

    [Header("Physics")]
    [HideInInspector] public Vector3 activeSpeed, passiveSpeed, currentGravity;
    public float forwardSpeed = 20f, strafeSpeed = 10f, backwardsSpeed = 5f, jumpSpeed = 15f; //Speeds
    public float forwardAcceleration = 15f, strafeAcceleration = 5f, backwardsAcceleration = 1f; //Accelerations
    public float staticDrag = 0.8f, groundDrag = 0.5f, airDrag = 0, mass = 2f, stickToGroundForce = 10f,
        speedOverflowReductionFactor = 0.7f; //Aproximadamente el porcentaje de velocidad que se pierde cada segundo

    [Header("Animation")]
    public WallMovementHelper wallMovement;
    [Range(0f, 1f)]
    public float runstepLengthen = 0.7f;
    public MouseLook mouseLook;
    public bool m_UseFovKick;
    public FOVKick m_FovKick = new FOVKick();
    public bool useHeadBob;
    public CurveControlledBob runHeadBob = new CurveControlledBob();
    public LerpControlledBob jumpHeadBob = new LerpControlledBob();
    public AnimationCurve stepIntervalCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    public float maxStepInterval;



    [Header("Sound")]
    public AudioClip[] stepsSound;    // an array of footstep sounds that will be randomly selected from.
    public AudioClip jumpSound;           // the sound played when character leaves the ground.
    public AudioClip landSound;           // the sound played when character touches back on ground.

    private Camera cam;
    private float yRotation;
    private Vector2 movementInput;
    private UnityEngine.CharacterController characterController;
    private CollisionFlags collFlags;
    public bool wasGrounded, jumpOrder, didJump;
    private Vector3 originalCamPos;
    private float stepCycle, nextStep;
    private AudioSource m_AudioSource;
    public float targetSpeed, acceleration, drag;

    #endregion

    public bool IsGrounded
    {
        get { return characterController.isGrounded; }

    }

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        instance = this;
        //capsule = GetComponent<CapsuleCollider>();

        currentGravity = Physics.gravity;
    }

    // Use this for initialization
    private void Start()
    {
        characterController = GetComponent<UnityEngine.CharacterController>();
        cam = Camera.main;
        originalCamPos = cam.transform.localPosition;
        m_FovKick.Setup(cam);
        //runHeadBob.Setup(cam, maxStepInterval);
        stepCycle = 0f;
        nextStep = stepCycle / 2f;
        m_AudioSource = GetComponent<AudioSource>();
        mouseLook.Init(transform, cam.transform);
    }

    private void Update()
    {
        if ((characterController.isGrounded || wallMovement.IsWallRunning) && Input.GetButtonDown("Jump"))
            jumpOrder = true;
    }

    private void FixedUpdate()
    {
        GetInput();


#if UNITY_EDITOR
        if (Cursor.lockState == CursorLockMode.None) return;
#endif
        RotateView();
        SetControlledSpeed();
        MovementStatesBehaviour();

        /*Here we could apply external forces
         ApplyExternalForces();*/

        //Here we MOVE THE CHARACTER
        collFlags = characterController.Move((activeSpeed + passiveSpeed) * Time.fixedDeltaTime); //NEON MOVES IN A TIME-INDEPENDENT FASHION

        ProgressStepCycle(activeSpeed.magnitude);
        UpdateCameraPosition(activeSpeed.magnitude);

    }

    private void MovementStatesBehaviour()
    {
        wallMovement.WallMovementCheck();

        //if (wallMovement.IsWallClimbing)
        //{
        //    activeSpeed.y = forwardSpeed;
        //}
        //else 
        if (characterController.isGrounded)
        {
            if (!wasGrounded) Land();

            if (jumpOrder)
                Jump(transform.up);
            else
                passiveSpeed.y = -stickToGroundForce;

        }
        else if (wallMovement.IsWallRunning)
        {
            //TODO: move *just* in the wall direction
            //TODO: wallrun only lasts a few seconds

            if (jumpOrder)
                Jump(wallMovement.wallHitInfo.normal.normalized + transform.up);
        }
        else
        {//Airborne
            Vector3 grav = currentGravity * (didJump && InputManager.instance.jumping ? .6f : 1f);
            passiveSpeed += grav * mass * PhysicsManager.instance.scaledFixedDeltaTime;
        }

        WallRunAngleCorrection();
        wasGrounded = characterController.isGrounded;

    }

    private void WallRunAngleCorrection()
    {
        mouseLook.wallRunTilt = Mathf.Lerp(mouseLook.wallRunTilt, //Current tilt
            wallMovement.WallRunAngleCorrection(), //Target tilt
            wallMovement.wallRunTiltSpeed); //Smooth factor
    }

    private void SetControlledSpeed()
    {
        /*if (characterController.isGrounded)
        {
            speed = cam.transform.forward.normalized * Vector3.ProjectOnPlane(speed, Vector3.up).magnitude + Vector3.up * speed.y;
        }*/

        if (movementInput.magnitude > 0)
        {
            drag = (characterController.isGrounded ? groundDrag : airDrag) * PhysicsManager.instance.scaledFixedDeltaTime;

            if (movementInput.y > 0)//taking in account movementInput is normalized: 
            {
                //Target speed & acceleration are lineal combinations of forward & strafe
                //currentTargetSpeed = forwardSpeed * Mathf.Abs(movementInput.y) + strafeSpeed * Mathf.Abs(movementInput.x);

                targetSpeed = forwardSpeed;
                acceleration = forwardAcceleration * Mathf.Abs(movementInput.y) + strafeAcceleration * Mathf.Abs(movementInput.x);
            }
            else if (movementInput.y < 0)
            {
                //currentTargetSpeed = backwardsSpeed * Mathf.Abs(movementInput.y) + strafeSpeed * Mathf.Abs(movementInput.x);
                targetSpeed = backwardsSpeed;
                acceleration = backwardsAcceleration * Mathf.Abs(movementInput.y) + strafeAcceleration * Mathf.Abs(movementInput.x);
            }
            else
            {
                targetSpeed = strafeSpeed;
                acceleration = strafeAcceleration;
            }
        }
        else
        {
            targetSpeed = forwardSpeed;
            acceleration = 0f;
            drag = (characterController.isGrounded ? staticDrag : airDrag);
        }

        Vector3 normalizedMovement = transform.forward * movementInput.y + transform.right * movementInput.x;

        // get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo, characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);

        normalizedMovement = Vector3.ProjectOnPlane(normalizedMovement, hitInfo.normal).normalized;
        //Debug.Log(normalizedMovement);

        Vector3 horizontalSpeed = Vector3.ProjectOnPlane(activeSpeed, hitInfo.normal);
        horizontalSpeed += normalizedMovement * acceleration * PhysicsManager.instance.scaledFixedDeltaTime;


        if (drag < horizontalSpeed.magnitude)
            horizontalSpeed -= horizontalSpeed.normalized * drag;

        else horizontalSpeed = Vector3.zero;

        if (horizontalSpeed.magnitude > targetSpeed)
        {
            horizontalSpeed = horizontalSpeed.normalized * Mathf.Lerp(horizontalSpeed.magnitude, targetSpeed, speedOverflowReductionFactor * PhysicsManager.instance.scaledFixedDeltaTime);
        }

        activeSpeed = new Vector3(horizontalSpeed.x, activeSpeed.y, horizontalSpeed.z);

    }

    private void Land()
    {
        didJump = false;
        //Debug.Log("landed");
        wallMovement.OnLand();
        StartCoroutine(jumpHeadBob.DoBobCycle());
        PlayLandingSound();
        passiveSpeed.y = 0f;
    }

    private void Jump(Vector3 direction)
    {
        //Debug.Log("Jump");
        passiveSpeed += direction.normalized * jumpSpeed;
        jumpOrder = false;
        didJump = true;

        //speed.y = jumpSpeed;
        PlayJumpSound();
    }

    private void GetInput()
    {
        // Read input
        float horizontal = InputManager.instance.strafe.x;
        float vertical = InputManager.instance.strafe.y;

        // set the desired speed to be walking or running
        movementInput = new Vector2(horizontal, vertical);

        // normalize input if it exceeds 1 in combined length:
        if (movementInput.sqrMagnitude > 1)
            movementInput.Normalize();

        // handle speed change to give an fov kick
        // only if the player is going to a run, is running and the fovkick is to be used
        if (m_UseFovKick && characterController.velocity.sqrMagnitude > 0)
        {
            StopAllCoroutines();
            StartCoroutine(m_FovKick.FOVKickDown());
        }
    }

    private void RotateView()
    {
        mouseLook.LookRotation(transform, cam.transform);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        //dont move the rigidbody if the character is on top of it
        if (collFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(characterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
    }

    private void ProgressStepCycle(float spd)
    {
        if (characterController.velocity.sqrMagnitude > 0 && (movementInput.x != 0 || movementInput.y != 0))
        {
            stepCycle += (characterController.velocity.magnitude + (spd * runstepLengthen)) *
                         Time.fixedDeltaTime;
        }

        if (stepCycle <= nextStep)
        {
            return;
        }

        nextStep = stepCycle + stepIntervalCurve.Evaluate(Mathf.Min(spd / targetSpeed, 1f)) * maxStepInterval;

        PlayFootStepAudio();
    }

    private void UpdateCameraPosition(float spd)
    {
        Vector3 newCameraPosition;
        if (!useHeadBob) return;

        if (characterController.velocity.magnitude > 0 && (characterController.isGrounded || wallMovement.IsWallRunning))
        {
            //cam.transform.localPosition = runHeadBob.DoHeadBob(characterController.velocity.magnitude + (spd * runstepLengthen), stepIntervalCurve.Evaluate(Mathf.Min(spd / targetSpeed, 1f)) * maxStepInterval);
            newCameraPosition = cam.transform.localPosition;
            newCameraPosition.y = cam.transform.localPosition.y - jumpHeadBob.Offset();
        }
        else
        {
            newCameraPosition = cam.transform.localPosition;
            newCameraPosition.y = originalCamPos.y - jumpHeadBob.Offset();
        }
        cam.transform.localPosition = newCameraPosition;
    }

    private void PlayLandingSound()
    {
        m_AudioSource.clip = landSound;
        m_AudioSource.Play();
        nextStep = stepCycle + .5f;
    }

    private void PlayFootStepAudio()
    {
        if (!(characterController.isGrounded || wallMovement.IsWallRunning 
            //|| wallMovement.IsWallClimbing
            ))
        {
            return;
        }
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, stepsSound.Length);
        m_AudioSource.clip = stepsSound[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        stepsSound[n] = stepsSound[0];
        stepsSound[0] = m_AudioSource.clip;
    }

    private void PlayJumpSound()
    {
        m_AudioSource.clip = jumpSound;
        m_AudioSource.Play();
    }
}