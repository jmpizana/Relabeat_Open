﻿using UnityEngine;
using System.Collections;
using System;

namespace MusicGeneration
{
    public class SongClip : ISong
    {
        public AudioClip clip;
        public int bpm;
        MusicManager manager;
        AudioSource source;

        public SongClip(MusicManager manager, AudioClip clip, int bpm)
        {
            this.manager = manager;
            this.clip = clip;
            this.bpm = bpm;

            InitializeSource();
        }

        private void InitializeSource()
        {
            source = GameObject.Instantiate<AudioSource>(manager.generationProperties.audioSourcePrefab, manager.transform);
            source.name = "Song Audio Source";
            source.clip = clip;
            source.outputAudioMixerGroup = manager.generationProperties.harmonyMixer;
            source.loop = true;
        }

        #region Runtime
        public void Play()
        {
            source.Play();
        }

        public void Pause()
        {
            source.Pause();
        }

        public void Resume()
        {
            source.UnPause();
        }

        public void Stop()
        {
            source.Stop();
        }

        public void Update(float playbackTime) { }

        public void Dispose()
        {
            Stop();
            GameObject.Destroy(source.gameObject);
        }
        #endregion

        #region Analysis
        public float GetInstantIntensity(float playbackTime)
        {
            throw new NotImplementedException();
        }

        private float GetInstantIntensity(float playbackTime, float timeInterval)
        {
            float intensity = 0f;

            if (playbackTime + timeInterval > clip.length)
            {
                //Get whatever time remains of the song and analyze the rest from the beginning

                float remainder = clip.length - playbackTime;

                intensity += GetInstantIntensity(playbackTime, remainder);
                intensity += GetInstantIntensity(0f, timeInterval - remainder);
                intensity *= 0.5f;
            }
            else
            {
                //Analyze as usual
                int dataSamples = Mathf.RoundToInt(clip.frequency * timeInterval);
                float[] data = new float[dataSamples]; //Its length has to correspond to the time interval
                float timeOffset = playbackTime;
                int offsetSamples = Mathf.RoundToInt(clip.frequency * timeOffset);

                if (!clip.GetData(data, offsetSamples)) throw new Exception(clip.name + " audio data could not be loaded");

                for (int i = 0; i < dataSamples; i++)
                {
                    intensity += data[i];
                }

                intensity = ((intensity / dataSamples) + 1f) * 0.5f;
            }

            return intensity;
        }

        public float GetInstantIntensityInLayer(MusicLayer layer, float playbackTime)
        {
            throw new NotImplementedException();
        }

        public float[] GetRawIntensitiesVector(float intervalsToSeconds, out float averageIntensity)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Getters and Setters
        private float secondsPerBeat = -1f;
        public float SecondsPerBeat()
        {
            if (secondsPerBeat < 0f)
                secondsPerBeat = 60f / bpm;

            return secondsPerBeat;
        }

        public float GetBPM()
        {
            return bpm;
        }

        public float GetDuration()
        {
            return clip.length;
        }
        #endregion
    }
}
