﻿using UnityEngine;
using System.Collections;
using System;
using PizanaUtility;

namespace MusicGeneration
{
    [System.Serializable]
    public abstract class ISamplesCollection<ParamsType> : ScriptableObject where ParamsType : IParams, new()
    {
        public ParamsType parameters;

        private static ISamplesCollection<ParamsType> preInitAsset;

        public virtual ISamplesCollection<ParamsType> Initialize<CollectionType>(ParamsType iParameters)
            where CollectionType : ISamplesCollection<ParamsType>
        {
            parameters = new ParamsType();
            parameters.AssignNotNull(iParameters);
            return this;
        }

        public static CollectionType InstantiateCollection<CollectionType>(ParamsType parameters, string path, string name)
            where CollectionType : ISamplesCollection<ParamsType>
        {
#if UNITY_EDITOR
            //TODO: test which is the most suitable method

            //STATIC REFERENCE METHOD: avoid GC errors
            preInitAsset = CreateInstance<CollectionType>().Initialize<CollectionType>(parameters);

            if (preInitAsset == null)
            {
                Debug.Log("Null asset attempting to be created");
                throw new UnityException("Null asset attempting to be created");
            }

            return ScriptableObjectUtility.CreatePreInitAsset(path, name, (CollectionType)preInitAsset);
            //STRAIGHTFORWARD METHOD: avoids more work
            //return ScriptableObjectUtility.CreateAsset<CollectionType>(path, name).Initialize<CollectionType>(parameters);
#else
            return null;
#endif
        }
    }
}