﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    [CreateAssetMenu(fileName = "NewSampleBMPCollection", menuName = "Music Generation/Sample BMP", order = 1), System.Serializable]
    public class SampleBPMCollection : ISamplesCollection<BPMCollectionParams> { }

    [System.Serializable]
    public class BPMCollectionParams : IParams
    {
        public int bpm;
        public SampleFamiliesPackage sampleFamilies;

        //public List<SampleFamilyCollection> nonEssentialSamplesFams;
        //public List<SampleFamilyCollection> essentialSamplesFams;

        public BPMCollectionParams(int bpm, SampleFamiliesPackage sampleFamilies)
        {
            this.bpm = bpm;
            this.sampleFamilies = sampleFamilies;
        }

        public BPMCollectionParams(int bpm, List<SampleFamily> nonEssentialSamplesFams, List<SampleFamily> essentialSamplesFams)
        {
            this.bpm = bpm;
            this.sampleFamilies = new SampleFamiliesPackage(essentialSamplesFams, nonEssentialSamplesFams);
        }

        public BPMCollectionParams() { }
    }
}
