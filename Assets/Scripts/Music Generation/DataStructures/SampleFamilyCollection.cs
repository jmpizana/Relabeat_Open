﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    [CreateAssetMenu(fileName = "SampleFamily", menuName = "Music Generation/Sample Family", order = 1), System.Serializable]
    public class SampleFamilyCollection : ISamplesCollection<FamilyCollectionParams>
    {

        //private static SampleFamilyCollection preInitAsset;
        /*
        public static SampleFamilyCollection InstantiateSampleFamily(List<Sample> samples, int bmp, MusicalMode mode, MusicalRhythm rhythm, string path, string name)
        {
            preInitAsset = CreateInstance<SampleFamilyCollection>().Initialize(samples, bmp, mode, rhythm);

            if (preInitAsset == null)
            {
                Debug.Log("Null asset attempting to be created");
                throw new UnityException("Null asset attempting to be created");
            }

            return ScriptableObjectUtility.CreatePreInitAsset(path, name, preInitAsset);
        }*/

        public Sample GetRandomSample()
        {
            return parameters.samples.GetRandom();
        }
    }

    [System.Serializable]
    public class FamilyCollectionParams : IParams
    {
        public List<Sample> samples;
        public int bmp = -1;
        public MusicMode mode = MusicMode.NONE;
        public MusicRhythm rhythm;

        public FamilyCollectionParams(List<Sample> samples, int bmp, MusicMode mode, MusicRhythm rhythm)
        {
            this.samples = samples;
            this.bmp = bmp;
            this.mode = mode;
            this.rhythm = rhythm;
        }

        public FamilyCollectionParams() { }
    }
}