﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    [CreateAssetMenu(fileName = "NewSampleModeCollection", menuName = "Music Generation/Sample Mode", order = 1), System.Serializable]
    public class SampleModeCollection : ISamplesCollection<ModeCollectionParams> { }

    [System.Serializable]
    public class ModeCollectionParams : IParams
    {
        public MusicMode mode;
        public BPMDictionary bpmsDictionary;


        public ModeCollectionParams(MusicMode mode, BPMDictionary bpmsDictionary)
        {
            this.mode = mode;
            this.bpmsDictionary = bpmsDictionary;
        }

        public ModeCollectionParams() { }
    }

    [System.Serializable]
    public class BPMDictionary: SerializableDictionary<int, SampleBPMCollection> { }
}
