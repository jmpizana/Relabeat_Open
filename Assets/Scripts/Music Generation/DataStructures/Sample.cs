﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    [CreateAssetMenu(fileName = "SampleData", menuName = "Music Generation/Sample Data", order = 1), System.Serializable]
    public class Sample : ISamplesCollection<SampleParameters>
    {
        /*private static Sample preInitAsset;

        public SampleParameters sampleData;

        public Sample Initialize(int bmp, AudioClip clip, MusicLayer layer, MusicalMode mode, MusicalRhythm rhythm)
        {
            this.sampleData = new SampleParameters(bmp, clip, layer, mode, rhythm);
            return this;
        }

        public static Sample InstantiateSample(int bmp, AudioClip clip, MusicLayer layer, MusicalMode mode,MusicalRhythm rhythm, string path, string name)
        {

            preInitAsset = CreateInstance<Sample>().Initialize(bmp, clip, layer, mode, rhythm);

            if (preInitAsset == null)
            {
                Debug.Log("Null asset attempting to be created");
                throw new UnityException("Null asset attempting to be created");
            }

            return ScriptableObjectUtility.CreatePreInitAsset(path, name, preInitAsset);
        }*/
    }

    [System.Serializable]
    public class SampleParameters : IParams
    {        

        public int bmp;
        public AudioClip clip;
        public MusicLayer layer;
        public MusicMode mode;
        public MusicRhythm rhythm;
        public float duration;

        public SampleParameters(int bmp, AudioClip clip, MusicLayer layer, MusicMode mode, MusicRhythm rhythm)
        {
            //if (bmp == 0) throw new UnityException("Trying to make a sample with bmp 0");

            this.bmp = bmp;
            this.clip = clip;
            this.layer = layer;
            this.mode = mode;
            this.rhythm = rhythm;
            this.duration = clip.length;
        }

        public SampleParameters() { }
    }

       
}

