﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * FUTURE PLANS - TODO
 * 
 * There are two mutually exclusive ways of composing the track that can be introduced:
 * - Classical song structure: Song segments that can be articulated between them with start and ending or just looped. Each one has subsets of all the samples selected for the song
 * - Reative music: Introduce narrative layers for the song that follow game events (i.e.: entering combat, or loading game).
 * 
 * Also, to give more differentiated sense to each composed piece:
 * - Make sample size smaller, right now it's 8 bars, and could be reduced at first instance to 4 and later those could be composed procedurally.
 * 
 * Generate Mixers for the different segments /sounds
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using PizanaUtility;
using System.Threading.Tasks;
using Random = UnityEngine.Random;

namespace MusicGeneration
{
    [System.Serializable]
    public class SongComposed : ISong
    {
        [Header("Parameters")]
        public SongParameters songParameters;

        //TODO: right now this pair is useless as we set stable segments of the same duration, so we could go with just SongSegments
        public TimeSegmentPair[] composedSegments;
        public int currentSegmentIndex = 0;

        public SongSegment introSegment, verseSegment, bridgeSegment, outroSegment;

        [Header("References")]
        public SamplesPackage melodySamples;
        public SamplesPackage harmonySamples;
        public SamplesPackage baseSamples;

        //TODO:if we want as many audiomixers as audiosources it will have to be done by hand
        public AudioMixerGroup melodyMixerGroup, harmonyMixerGroup, beatMixerGroup;

        //public List<SampleSourcePair> meloSources;
        //public List<SampleSourcePair> accoSources;
        //public List<SampleSourcePair> baseSources;

        SongSegmentSources currentSegSources, nextSegSources;
        Queue<SongSegmentSources> previousSegSources;
        //SongSegmentSources[] previousSegSources;
        MusicGenerator context;
        MusicGenerationProperties generationParameters;
        double songStartTime;
        int loops = 0;
        bool ending = false, switching = false, stopped = true;
        bool loadingNextSegmentAsync = false;
        Transform audiosParent;

        #region Properties

        private int NextSegmentIndex
        {
            get
            {
                return (int)(((float)currentSegmentIndex + 1f) % (float)composedSegments.Length);
            }
        }

        private float secondsPerBeat = -1f;
        public float SecondsPerBeat()
        {
            if (secondsPerBeat < 0f)
                secondsPerBeat = 60f / songParameters.bpm;

            return secondsPerBeat;
        }

        #endregion

        public SongComposed(SamplesPackage melodySamples, SamplesPackage accompanimentSamples, SamplesPackage baseSamples,
            AudioMixerGroup mainMixerGroup, AudioMixerGroup accompanimentMixerGroup, AudioMixerGroup baseMixerGroup,
            MusicGenerator context, SongParameters songParameters)
        {
            this.melodySamples = melodySamples;
            this.harmonySamples = accompanimentSamples;
            this.baseSamples = baseSamples;
            this.melodyMixerGroup = mainMixerGroup;
            this.harmonyMixerGroup = accompanimentMixerGroup;
            this.beatMixerGroup = baseMixerGroup;
            this.context = context;
            this.songParameters = songParameters;
            this.generationParameters = context.manager.generationProperties;

            previousSegSources = new Queue<SongSegmentSources>(generationParameters.previousSegmentSourcesStored + 1);

            //if (!Initialize(songParameters)) throw new UnityException("Song could not be initialized");

            audiosParent = new GameObject("AudiosParent").transform;
            audiosParent.parent = context.manager.transform;

            GenerateSongStructure();

            currentSegmentIndex = 0;

            currentSegSources = InitializeSongSegmentSources(composedSegments[0].second);
            LoadNextSegmentSourceAsync(composedSegments[1].second);
        }

        #region Generation

        void GenerateSongStructure()
        {
            int segmentAmount = songParameters.SegmentAmount;

            if (segmentAmount <= 0)
                throw new UnityException("Trying to generate a song shorter than its longest Sample");
            if (segmentAmount <= 2) throw new UnityException("Trying to generate a song shorter than 3 times its longest Sample");


            //melodySamples.Shuffle();
            //Stack<Sample> melSamplesStack = new Stack<Sample>(melodySamples);

            //TODO: make system able to admit many main segments
            InitializeSegmentStructure(out verseSegment, generationParameters.mainSegmentAmounts, melodySamples, SongSegmentType.VERSE);
            InitializeSegmentStructure(out bridgeSegment, generationParameters.bridgeSegmentAmounts, melodySamples, SongSegmentType.BRIDGE, false);
            InitializeSegmentStructure(out introSegment, generationParameters.introSegmentAmounts, verseSegment.melodySamples, SongSegmentType.INTRO, false);
            InitializeSegmentStructure(out outroSegment, generationParameters.endingSegmentAmounts, verseSegment.melodySamples, SongSegmentType.OUTRO, false);

            //SEGMENT COMPOSITION

            int remainingSegments = segmentAmount;
            composedSegments = new TimeSegmentPair[segmentAmount];

            //First we fill al the song with the main part
            for (int i = 0; i < segmentAmount; i++)
                composedSegments[i] = new TimeSegmentPair(i * songParameters.SegmentsToSeconds, verseSegment);


            composedSegments[0] = new TimeSegmentPair(0f, introSegment);
            remainingSegments--;

            if (remainingSegments >= 3 /*TODO: && add random value*/)
            {
                composedSegments[segmentAmount - 1] = new TimeSegmentPair((segmentAmount - 1f) * songParameters.SegmentsToSeconds, outroSegment);
                remainingSegments--;
            }

            if (remainingSegments >= 3 /*TODO: && add random value*/)
            {
                //ADDING BRIDGE ON THE MIDDLE OF THE SONG

                int middle = Mathf.CeilToInt((float)segmentAmount / 2f);

                composedSegments[middle] = new TimeSegmentPair(middle * songParameters.SegmentsToSeconds, bridgeSegment);
            }

            //the end?
        }

        void InitializeSegmentStructure(out SongSegment songSegment, MusicGenerationProperties.SegmentAmounts amounts, SamplesPackage samples, SongSegmentType type, bool essentialFirst = true)
        {
            int melodyAmount = Random.Range(amounts.melodyAmounts.min, Mathf.Min(amounts.melodyAmounts.max, samples.Count) + 1);
            int accAmount = Random.Range(amounts.accoAmounts.min, Mathf.Min(amounts.accoAmounts.max, harmonySamples.Count) + 1);
            int baseAmount = Random.Range(amounts.baseAmounts.min, Mathf.Min(amounts.baseAmounts.max, baseSamples.Count) + 1);

            songSegment = new SongSegment(
                new List<Sample>(samples.GetSamplesAmountRandom(melodyAmount, essentialFirst)),
                new List<Sample>(harmonySamples.GetSamplesAmountRandom(accAmount, essentialFirst)),
                new List<Sample>(baseSamples.GetSamplesAmountRandom(baseAmount, essentialFirst)),
                null, type
                );

        }

        void InitializeSegmentStructure(out SongSegment songSegment, MusicGenerationProperties.SegmentAmounts amounts, List<Sample> samples, SongSegmentType type, bool essentialFirst = true)
        {
            int melodyAmount = Random.Range(amounts.melodyAmounts.min, Mathf.Min(amounts.melodyAmounts.max, samples.Count) + 1);
            int accAmount = Random.Range(amounts.accoAmounts.min, Mathf.Min(amounts.accoAmounts.max, harmonySamples.Count) + 1);
            int baseAmount = Random.Range(amounts.baseAmounts.min, Mathf.Min(amounts.baseAmounts.max, baseSamples.Count) + 1);

            songSegment = new SongSegment(
                new List<Sample>(samples.GetRandomSubset(melodyAmount)),
                new List<Sample>(harmonySamples.GetSamplesAmountRandom(accAmount, essentialFirst)),
                new List<Sample>(baseSamples.GetSamplesAmountRandom(baseAmount, essentialFirst)),
                null, type
                );

        }

        /*void InitializeSegmentStructureStack(out SongSegment songSegment, MusicGenerationProperties.SegmentAmounts amounts, Stack<Sample> samplesStack, SongSegmentType type)
        {
            int melodyAmount = Random.Range(amounts.melodyAmounts.min, Mathf.Min(amounts.melodyAmounts.max, samplesStack.Count) + 1);
            int accAmount = Random.Range(amounts.accoAmounts.min, Mathf.Min(amounts.accoAmounts.max, harmonySamples.Count) + 1);
            int baseAmount = Random.Range(amounts.baseAmounts.min, Mathf.Min(amounts.baseAmounts.max, baseSamples.Count) + 1);

            songSegment = new SongSegment(new List<Sample>(samplesStack.PopMany(melodyAmount)),
                new List<Sample>(harmonySamples.GetRandomSubset(accAmount)),
                new List<Sample>(baseSamples.GetRandomSubset(baseAmount)),
                null,
                type);
        }

        void InitializeSegmentStructure(out SongSegment songSegment, MusicGenerationProperties.SegmentAmounts amounts, List<Sample> samplesList, SongSegmentType type)
        {
            int melodyAmount = Random.Range(amounts.melodyAmounts.min, Mathf.Min(amounts.melodyAmounts.max, samplesList.Count) + 1);
            int accAmount = Random.Range(amounts.accoAmounts.min, Mathf.Min(amounts.accoAmounts.max, harmonySamples.Count) + 1);
            int baseAmount = Random.Range(amounts.baseAmounts.min, Mathf.Min(amounts.baseAmounts.max, baseSamples.Count) + 1);

            songSegment = new SongSegment(new List<Sample>(samplesList.GetRandomSubset(melodyAmount)),
                new List<Sample>(harmonySamples.GetRandomSubset(accAmount)),
                new List<Sample>(baseSamples.GetRandomSubset(baseAmount)),
                null,
                type);
        }*/

        #endregion

        #region Runtime

        async public void Play()
        {
            currentSegmentIndex = 0;
            if (currentSegSources == null) throw new UnityException("Trying to play a song without initializing the first segment sources");

            songStartTime = AudioSettings.dspTime + context.manager.generationProperties.startSongTimeBuffer;
            currentSegSources.PlayScheduled(songStartTime);

            await new WaitForSeconds(context.manager.generationProperties.startSongTimeBuffer);
            stopped = false;
        }

        private void SetTimeAudioSourceList(List<AudioSource> listToPlay, float time)
        {
            for (int i = 0; i < listToPlay.Count; i++)
            {
                listToPlay[i].time = time;
            }
        }

        public void Pause()
        {
            currentSegSources.Pause();
        }

        public void Resume()
        {
            currentSegSources.Resume();
        }

        public void Stop()
        {
            currentSegSources.Stop();
            if (switching) nextSegSources.Stop();

            switching = false;
            ending = false;
            stopped = true;

            loops = 0;
            currentSegmentIndex = 0;
        }

        public void Update(float playbackTime)
        {
            if (ending || switching || stopped) return;
            CheckSegmentSwitch(playbackTime);
        }


        private void CheckSegmentSwitch(float playbackTime)
        {
            float remainingTime;

            //If song is going to end and not loop (ending segment has always a duration of 1)
            if (!songParameters.looping && currentSegmentIndex == composedSegments.Length - 1)
            {
                remainingTime = songParameters.maxSampleDuration - (playbackTime % songParameters.maxSampleDuration);

                if (remainingTime < songParameters.nextSegmentTimeBuffer)
                {
                    Debug.Log("Ending song");
                    ending = true;
                    context.manager.Invoke("StopSong", remainingTime);
                }
            }

            int nextSegmentIndex = NextSegmentIndex;
            Pair<float, SongSegment> nextSegment = composedSegments[nextSegmentIndex];

            //Remaining time: times looped + next segment start time - actual playback time
            remainingTime = Mathf.Abs(loops * songParameters.SegmentsToSeconds * composedSegments.Length + nextSegment.first - playbackTime);
            //Debug.Log(remainingTime);
            if (remainingTime < songParameters.nextSegmentTimeBuffer)
            {
#if UNITY_EDITOR
                if(context.manager.debugMessages)
                    Debug.Log("SONG MANAGER: switching song segment");
#endif
                switching = true;
                SwitchSegments(nextSegment.second, remainingTime);
            }
        }

        async private void SwitchSegments(SongSegment nextSegment, float remainingTime)
        {

            double nextSegmentStartTime = AudioSettings.dspTime + (double)remainingTime;

            currentSegSources.SetScheduledEndTime(nextSegmentStartTime - double.Epsilon);

            //Debug.Log(remainingTime + " || " + (context.manager.playbackTime + remainingTime));
            //Debug.Log(nextSegmentStartTime + " || " + (nextSegmentStartTime + remainingTime));

            while (loadingNextSegmentAsync) Debug.Log("Waiting for next segment to load"); //We have to forcibly wait to the next segment sources to be loaded

            nextSegSources.PlayScheduled(nextSegmentStartTime);

            //And then we assign the new variables.
            //context.manager.Invoke(nameof(context.manager.EndSwitchingSongSegments), remainingTime);
            await new WaitForSeconds(remainingTime);
            context.manager.EndSwitchingSongSegments();
        }

        public void EndSwitchingSongSegments()
        {
            SongSegment currentSegmentInfo = composedSegments[currentSegmentIndex].second;

            currentSegmentIndex = NextSegmentIndex;
            //If there is not a copy of this sources already in the backup queue
            if (!previousSegSources.HasSegmentAudiosAlready(currentSegmentInfo))
            {
                previousSegSources.Enqueue(currentSegSources);
                if (previousSegSources.Count > generationParameters.previousSegmentSourcesStored)
                    previousSegSources.Dequeue().Dispose();
            }

            currentSegSources = nextSegSources;
            LoadNextSegmentSourceAsync(composedSegments[NextSegmentIndex].second);
            if (NextSegmentIndex == 0) loops++;

            switching = false;
        }

        async private void LoadNextSegmentSourceAsync(SongSegment segment)
        {
            loadingNextSegmentAsync = true;

            //Task<SongSegmentSources> task = new Task<SongSegmentSources>(() => InitializeSongSegmentSources(segment));
            //task.Start();
            //nextSegSources = await task;

            nextSegSources = await InitializeSongSegmentSourcesAsync(segment);
            loadingNextSegmentAsync = false;
        }

        async Task<SongSegmentSources> InitializeSongSegmentSourcesAsync(SongSegment segment)
        {
            AudioSource[] beatSources = InitializeSegmentLayerSources(segment.beatSamples, beatMixerGroup);
            AudioSource[] melodySources = InitializeSegmentLayerSources(segment.melodySamples, melodyMixerGroup);
            AudioSource[] harmonySources = InitializeSegmentLayerSources(segment.harmonySamples, harmonyMixerGroup);

            return new SongSegmentSources(segment, melodySources, harmonySources, beatSources);
        }

        private SongSegmentSources InitializeSongSegmentSources(SongSegment segment)
        {
            AudioSource[] beatSources = InitializeSegmentLayerSources(segment.beatSamples, beatMixerGroup);
            AudioSource[] melodySources = InitializeSegmentLayerSources(segment.melodySamples, melodyMixerGroup);
            AudioSource[] harmonySources = InitializeSegmentLayerSources(segment.harmonySamples, harmonyMixerGroup);

            return new SongSegmentSources(segment, melodySources, harmonySources, beatSources);
        }

        private AudioSource[] InitializeSegmentLayerSources(List<Sample> samples, AudioMixerGroup mixerGroup)
        {
            AudioSource[] sources = new AudioSource[samples.Count];

            for (int i = 0; i < samples.Count; i++)
            {
                //parameters like PlayOnAwake set on prefab
                sources[i] = GameObject.Instantiate<AudioSource>(context.manager.generationProperties.audioSourcePrefab, audiosParent);
                sources[i].gameObject.name = mixerGroup.name + " --> " + samples[i].parameters.clip.name;
                sources[i].clip = samples[i].parameters.clip;
                sources[i].outputAudioMixerGroup = mixerGroup;
                sources[i].loop = true;
            }
            return sources;
        }

        public void Dispose()
        {
            Stop();

            previousSegSources.ForEach((SongSegmentSources s) => s.Dispose());
            currentSegSources.Dispose();
            nextSegSources.Dispose();
        }
        #endregion

        #region Analysis
        public float GetInstantIntensity(float playbackTime)
        {
            float frequency = baseSamples.essential[0].parameters.clip.frequency;
            float intensity = this.GetAudioIntensityForInterval(playbackTime, 2f / frequency); //We take two as a safety measure
            return intensity;
        }

        public float GetInstantIntensityInLayer(MusicLayer layer, float time)
        {
            if (time < 0) return 0f;
            //TODO: check if this works

            //first we assess the correct segment and then its samples
            float frequency = baseSamples.essential[0].parameters.clip.frequency;
            int selectedSegmentIndex = Mathf.FloorToInt(songParameters.SecondsToSegments * time);
            float innerTime = time % this.songParameters.SegmentsToSeconds;

            if (selectedSegmentIndex >= composedSegments.Length) return 0f;

            SongSegment selectedSegment = composedSegments[selectedSegmentIndex].second;

            List<Sample> selectedLayerSamples = selectedSegment.beatSamples;

            switch (layer)
            {
                case MusicLayer.BASE:
                    selectedLayerSamples = selectedSegment.beatSamples;
                    break;

                case MusicLayer.HARMONY:
                    selectedLayerSamples = selectedSegment.harmonySamples;
                    break;

                case MusicLayer.MELODY:
                    selectedLayerSamples = selectedSegment.melodySamples;
                    break;

                default:
                    selectedLayerSamples = selectedSegment.beatSamples;
                    break;
            }
            float intensity = TrackSongExtension.GetAudioIntensityForIntervalInLayer(selectedLayerSamples, time, 2f / frequency); //We take two as a safety measure

            return intensity;
        }

        public float[] GetRawIntensitiesVector(float intervalsToSeconds, out float averageIntensity)
        {
            float totalDuration = songParameters.duration;
            float timePerInterval = intervalsToSeconds;

            int intervalsAmount = Mathf.CeilToInt(totalDuration / timePerInterval);

            float[] intensities = new float[intervalsAmount];
            averageIntensity = 0;

            for (int i = 0; i < intervalsAmount; i++)
            {
                intensities[i] = this.GetAudioIntensityForInterval(i * timePerInterval, timePerInterval);
                averageIntensity += intensities[i];
            }

            averageIntensity /= intervalsAmount;//TODO: average intensities should be measured on a per-song segment basis, not for the entire song

            return intensities;
        }

        public float GetBPM()
        {
            return songParameters.bpm;
        }

        public float GetDuration()
        {
            return songParameters.duration;
        }
        #endregion
    }
}
