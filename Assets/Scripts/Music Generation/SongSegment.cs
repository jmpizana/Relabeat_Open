﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
*/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;
using System.Runtime.CompilerServices;
using System;
using System.Threading.Tasks;

namespace MusicGeneration
{
    [System.Serializable]
    public class SongSegment
    {
        public List<Sample> melodySamples;
        public List<Sample> harmonySamples;
        public List<Sample> beatSamples;

        public SongEffect[] effects;
        public SongSegmentType type;

        public SongSegment(List<Sample> melody, List<Sample> harmony, List<Sample> beat, SongEffect[] effects, SongSegmentType type)
        {
            this.melodySamples = melody;
            this.harmonySamples = harmony;
            this.beatSamples = beat;
            this.effects = effects;
            this.type = type;
        }
    }

    [System.Serializable]
    public class SongSegmentSources
    {
        public SongSegment segment;

        public AudioSource[] harmonySources;
        public AudioSource[] melodySources;
        public AudioSource[] beatSources;

        public SongSegmentSources(SongSegment segment, AudioSource[] melodySources, AudioSource[] harmonySources, AudioSource[] beatSources)
        {
            this.segment = segment;
            this.melodySources = melodySources;
            this.harmonySources = harmonySources;
            this.beatSources = beatSources;
        }

        public void PlayScheduled(double songStartTime)
        {
            beatSources.ForEach((AudioSource a) => a.PlayScheduled(songStartTime));
            melodySources.ForEach((AudioSource a) => a.PlayScheduled(songStartTime));
            harmonySources.ForEach((AudioSource a) => a.PlayScheduled(songStartTime));
        }

        public void Stop()
        {
            beatSources.ForEach((AudioSource a) => a.Stop());
            melodySources.ForEach((AudioSource a) => a.Stop());
            harmonySources.ForEach((AudioSource a) => a.Stop());
        }

        public void Pause()
        {
            beatSources.ForEach((AudioSource a) => a.Pause());
            melodySources.ForEach((AudioSource a) => a.Pause());
            harmonySources.ForEach((AudioSource a) => a.Pause());
        }

        public void Resume()
        {
            beatSources.ForEach((AudioSource a) => a.UnPause());
            melodySources.ForEach((AudioSource a) => a.UnPause());
            harmonySources.ForEach((AudioSource a) => a.UnPause());
        }

        public void SetScheduledEndTime(double scheduledEndTime)
        {
            beatSources.ForEach((AudioSource a) => a.SetScheduledEndTime(scheduledEndTime));
            melodySources.ForEach((AudioSource a) => a.SetScheduledEndTime(scheduledEndTime));
            harmonySources.ForEach((AudioSource a) => a.SetScheduledEndTime(scheduledEndTime));
        }

        public void Dispose()
        {
            beatSources.ForEach((AudioSource a) => GameObject.Destroy(a.gameObject));
            melodySources.ForEach((AudioSource a) => GameObject.Destroy(a.gameObject));
            harmonySources.ForEach((AudioSource a) => GameObject.Destroy(a.gameObject));
        }
    }

    [System.Serializable]
    public class SampleSourcePair : Pair<Sample, AudioSource>
    {
        public SampleSourcePair(Sample first, AudioSource second) : base(first, second)
        {
        }
    }
}
