﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    public static class MusicConstants
    {
        private static string[] layerDefaultNames = { "Base", "Melody", "Accompaniment" };

        public static string[] LayerDefaultNames
        {
            get { return layerDefaultNames; }
        }

        private static string[] essentialityDenominators = { "Essential", "NonEssential" };

        public static string[] EssentialityDenominators
        {
            get { return essentialityDenominators; }
        }

        static string[] layerNames = null, modeNames = null;

        public static string[] LayerNames
        {
            get
            {
                if (layerNames == null) layerNames = Enum.GetNames(typeof(MusicLayer));
                return layerNames;
            }
        }

        public static string[] ModeNames
        {
            get
            {
                if (modeNames == null) modeNames = Enum.GetNames(typeof(MusicMode));
                return modeNames;
            }
        }
    }


    public enum SongSegmentType
    {
        VERSE, CHORUS, INTRO, OUTRO, BRIDGE, VERSE_STRONG
    }

    public enum MusicMode
    {
        NONE
    }

    public enum MusicRhythm
    {
        _4_4, _3_4
    }

    public enum MusicLayer
    {
        BASE, HARMONY, MELODY, ONESHOTS
    }

    [Serializable]
    public struct LayerNameEnumValuePair
    {
        public string layerName;
        public MusicLayer layer;
    }

    [Serializable]
    public struct SampleFamiliesPackage
    {
        public List<SampleFamily> essential;
        public List<SampleFamily> nonEssential;

        public SampleFamiliesPackage(List<SampleFamily> essential, List<SampleFamily> nonEssential)
        {
            this.essential = essential;
            this.nonEssential = nonEssential;
        }
    }

    [Serializable]
    public struct SamplesPackage
    {
        public List<Sample> essential;
        public List<Sample> nonEssential;

        public SamplesPackage(List<Sample> essential, List<Sample> nonEssential)
        {
            this.essential = essential;
            this.nonEssential = nonEssential;
        }

        public int Count
        {
            get { return essential.Count + nonEssential.Count; }
        }

        public List<Sample> GetSamplesAmountRandom(int amount, bool essentialFirst = true)
        {
            List<Sample> value = new List<Sample>(amount);

            if (essentialFirst)
            {
                if (amount <= essential.Count)
                    value.AddRange(essential.GetRandomSubset(amount));
                else
                {
                    value.AddRange(essential);
                    int remaining = amount - essential.Count;
                    value.AddRange(nonEssential.GetRandomSubset(remaining));
                }
            }
            else
            {
                List<Sample> allSamples = new List<Sample>(essential);
                allSamples.AddRange(nonEssential);

                value.AddRange(allSamples.GetRandomSubset(amount));
            }

            

            return value;
        }
    }

    [Serializable]
    public struct SongParameters
    {
        public int bpm;
        public float duration, maxSampleDuration, nextSegmentTimeBuffer;
        [SearchableEnum] public MusicMode mode;
        [SearchableEnum] public MusicRhythm rhythm;
        public bool looping;
        public int segmentAmount;
        //public AnimationCurve segmentSwitchCurve;

        [Tooltip("The amounf of previous segment AudioSources that will be stored for possible future use")]
        public int previousSegmentSourcesStored;

        bool segmentAmountInitialized;

        public SongParameters(int bmp, float duration, float maxSampleDuration, float nextSegmentTimeBuffer, MusicMode mode, MusicRhythm rhythm, int previousSegmentSourcesStored, bool looping
            //, AnimationCurve segmentSwitchCurve
            )
        {
            this.bpm = bmp;
            this.duration = duration;
            this.maxSampleDuration = maxSampleDuration;
            this.nextSegmentTimeBuffer = nextSegmentTimeBuffer;
            this.mode = mode;
            this.rhythm = rhythm;
            this.looping = looping;
            this.previousSegmentSourcesStored = previousSegmentSourcesStored;
            //this.segmentSwitchCurve = segmentSwitchCurve;

            segmentAmount = -1;
            segmentAmountInitialized = false;

        }

        public int SegmentAmount
        {
            get
            {
                if (!segmentAmountInitialized)
                {
                    segmentAmount = Mathf.FloorToInt(duration / maxSampleDuration);
                    segmentAmountInitialized = true;
                }
                return segmentAmount;
            }
        }

        public float SegmentsToSeconds
        {
            get { return maxSampleDuration; }
        }

        public float SecondsToSegments
        {
            get { return 1 / maxSampleDuration; }
        }
    }    

    [Serializable]
    public class TimeSegmentPair : Pair<float, SongSegment>
    {
        public TimeSegmentPair(float first, SongSegment second) : base(first, second) { }
    }
}

