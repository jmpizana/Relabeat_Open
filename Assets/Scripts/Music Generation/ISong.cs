﻿using UnityEngine;
using System.Collections;

namespace MusicGeneration
{
    public interface ISong
    {
        #region Runtime
        void Play();
        void Pause();
        void Resume();
        void Stop();
        void Update(float playbackTime);
        void Dispose();
        #endregion

        #region Analysis
        float[] GetRawIntensitiesVector(float intervalsToSeconds, out float averageIntensity);
        float GetInstantIntensity(float playbackTime);
        float GetInstantIntensityInLayer(MusicLayer layer, float playbackTime);
        #endregion

        #region Getters & Setters
        float SecondsPerBeat();
        float GetBPM();
        float GetDuration();
        #endregion
    }
}
