﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO:
 * 1.- Create indispensable and immutable samples (i.e: el bombo de la base)
 * 3.- Differentiate between maxComposingSamples and maxSimultaneousSamples
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;
using Random = UnityEngine.Random;
using System.Threading.Tasks;

namespace MusicGeneration
{
    public class MusicGenerator
    {
        public MusicManager manager;
        MusicGenerationProperties genPars;

        public MusicGenerator(MusicManager manager)
        {
            this.manager = manager;
            this.genPars = manager.generationProperties;
        }

        async public Task<SongComposed> GenerateSongAsync()
        {
            /*TODO: update the description to current state
             * 
             * Algorithm breakdown:     
             * 1.- We select the base layer in order to work with it. We set the song bmp to the base bmp. Between 3 and 5 samples
             * 2.- We select the main voice layer getting a sample that sets the mode and choosing the rest accordingly. Between 1 and 2 samples.
             * 3.- We select the accompaniment layer with the given restrictions. Between 2 and 4 samples.
             
             * This generates a main verse-chorus block, that can be looped
             
             */

            SongComposed newSong;
            int bpm;
            MusicMode mode;
            //float maxSampleDuration = 0;
            float multiplier = genPars.sampleBufferMultiplier;
            MusicGenerationProperties.SegmentAmounts segment = genPars.mainSegmentAmounts; //We take amounts of the most populated segment

            //int baseLayerAmount = (int)((float)segment.baseAmounts.max * multiplier);
            //int meloLayerAmount = (int)((float)segment.melodyAmounts.max * multiplier);
            //int harmLayerAmount = (int)((float)(segment.accoAmounts.max * multiplier));

            int baseLayerAmount = (int)(Random.Range(segment.baseAmounts.min, segment.baseAmounts.max + 1) * multiplier);
            int meloLayerAmount = (int)(Random.Range(segment.melodyAmounts.min, segment.melodyAmounts.max + 1) * multiplier);
            int harmLayerAmount = (int)(Random.Range(segment.accoAmounts.min, segment.accoAmounts.max + 1) * multiplier);


            SamplesPackage baseSamples, mainSamples, accoSamples;

            MusicGenerationProperties propert = manager.generationProperties;



            SampleModeCollection selectedModeMelody =
                propert.library.parameters
                .meloLayer.parameters
                .modesDictionary.GetRandom();

            SampleBPMCollection selectedBPMBase =
                propert.library.parameters
                .baseLayer.parameters
                .modesDictionary.GetRandom().parameters
                .bpmsDictionary.GetRandom();

            bpm = selectedBPMBase.parameters.bpm;
            mode = selectedModeMelody.parameters.mode;

            //List<SampleFamilyCollection> selectedAccompanimentFamilies =
            //    propert.library.parameters.accoLayer.parameters
            //    .modesDictionary[mode].parameters
            //    .bpmsDictionary[bpm].parameters
            //    .samplesFamilies;

            BPMCollectionParams baseParams = selectedBPMBase.parameters,
                meloParams = selectedModeMelody.parameters.bpmsDictionary[bpm].parameters,
                harmoparams = propert.library.parameters.accoLayer.parameters
                .modesDictionary[mode].parameters
                .bpmsDictionary[bpm].parameters;

            FillLayerSamples(meloLayerAmount, out mainSamples, meloParams.sampleFamilies.essential, meloParams.sampleFamilies.nonEssential
                //, ref maxSampleDuration
                );
            FillLayerSamples(baseLayerAmount, out baseSamples, baseParams.sampleFamilies.essential, baseParams.sampleFamilies.nonEssential
                //, ref maxSampleDuration
                );
            FillLayerSamples(harmLayerAmount, out accoSamples, harmoparams.sampleFamilies.essential, harmoparams.sampleFamilies.nonEssential
                //, ref maxSampleDuration
                );

            int duration = Mathf.RoundToInt(manager.generationProperties.desiredSongMinutes * 60f / genPars.maxSampleDuration);

            newSong = new SongComposed(mainSamples, accoSamples, baseSamples,
                propert.melodyMixer, propert.harmonyMixer, propert.beatMixer,
                this, new SongParameters(bpm, duration * genPars.maxSampleDuration, genPars.maxSampleDuration,
                genPars.nextSegmentTimeBuffer, MusicMode.NONE, MusicRhythm._4_4,
                genPars.previousSegmentSourcesStored, true
                //, genPars.segmentSwitchCurve
                ));

            // manager.Song = newSong;
            return newSong;
        }

        private void FillLayerSamples(int layerSamplesAmount, out SamplesPackage targetSamples,
            List<SampleFamily> essentialSourceSamples, List<SampleFamily> nonEssentialSourceSamples
            //, ref float maxSampleDuration
            )
        {
            List<Sample> essentialList;
            List<Sample> nonEssentialList;

            if (essentialSourceSamples.Count >= layerSamplesAmount)
            {
                essentialList = new List<Sample>(layerSamplesAmount);
                FillLayerSamples(layerSamplesAmount, ref essentialList, essentialSourceSamples
                    //, ref maxSampleDuration
                    );
                targetSamples = new SamplesPackage(essentialList, null);
            }
            else
            {
                int nonEssentialAmount = layerSamplesAmount - essentialSourceSamples.Count;
                essentialList = new List<Sample>(essentialSourceSamples.Count);
                nonEssentialList = new List<Sample>(nonEssentialAmount);

                FillLayerSamples(essentialSourceSamples.Count, ref essentialList, essentialSourceSamples
                    //, ref maxSampleDuration
                    );
                FillLayerSamples(nonEssentialAmount, ref nonEssentialList, nonEssentialSourceSamples
                    //, ref maxSampleDuration
                    );
                targetSamples = new SamplesPackage(essentialList, nonEssentialList);
            }
        }

        /// <summary>
        /// Fills targetSamples with the selected amount of different sourcesamples
        /// </summary>
        /// <param name="layerSamplesAmount"></param>
        /// <param name="targetSamples"></param>
        /// <param name="sourceSamples"></param>
        private void FillLayerSamples(int layerSamplesAmount, ref List<Sample> targetSamples, List<SampleFamily> sourceSamples
            //, ref float maxSampleDuration
            )
        {
            if (sourceSamples == null || sourceSamples.Count == 0)
            {
                return;
            }

            SampleFamily[] availableSamples = new SampleFamily[sourceSamples.Count];
            sourceSamples.CopyTo(availableSamples);

            //In the special case source is as long as target we just copy it on target
            if (sourceSamples.Count == layerSamplesAmount)
            {
                for (int i = 0; i < layerSamplesAmount; i++) targetSamples.Add(sourceSamples[i].GetRandomSample());
                return;
            }

            //if  (sourceSamples.Count < layerSamplesAmount)
            //{
            //    int times = layerSamplesAmount / sourceSamples.Count;

            //    for (int i = 0; i < times; i++)
            //    {
            //        for (int j = 0; j < sourceSamples.Count; j++)
            //        {
            //            targetSamples.Add(sourceSamples[j].GetRandomSample());
            //        }
            //    }

            //    layerSamplesAmount = layerSamplesAmount % sourceSamples.Count;
            //}

            for (int i = 0, pickedInRotation = 0; i < layerSamplesAmount; i++, pickedInRotation++)
            {
                if (pickedInRotation >= availableSamples.Length) pickedInRotation = 0;
                int lastPickable = availableSamples.Length - pickedInRotation - 1;
                int chosenSampleIndex = Random.Range(0, lastPickable + 1);

                Sample selectedSample = availableSamples[chosenSampleIndex].GetRandomSample();

                targetSamples.Add(selectedSample);

                //if (selectedSample.parameters.duration > maxSampleDuration)
                //    maxSampleDuration = selectedSample.parameters.duration;

                if (chosenSampleIndex < lastPickable)
                {
                    //If 
                    SampleFamily swapper = availableSamples[chosenSampleIndex];
                    availableSamples[chosenSampleIndex] = availableSamples[lastPickable];
                    availableSamples[lastPickable] = swapper;
                }
            }

            //We don't need to return as lists are passed by reference 
        }


    }
}
