﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrackGeneration
{
    public class GapDanger : TrackDanger
    {
        public override void Initialize(TrackTile tile, TrackGenerationProperties generationParameters)
        {
            tile.parameters.generateFloor = false;
        }

    }
}