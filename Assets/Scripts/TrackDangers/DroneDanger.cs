﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TrackGeneration
{
    [RequireComponent(typeof(MeshRenderer))]
    public abstract class DroneDanger : TrackDanger
    {
        [Header("General Drone Parameters")]
        public Vector2 innerRectangleDimensionProportions = new Vector2(0.5f, 0.5f);

        protected Vector2 localCenter;

        protected Vector3 logicalLocalScale;

        [Header("Editor Debug Parameters")]
        public Color wireColor = Color.white;

        public override void Initialize(TrackTile tile, TrackGenerationProperties generationParameters)
        {
            PlaceAtStartOfTile(generationParameters);

            logicalLocalScale = new Vector3(tile.tileSectionDimensions.x, tile.tileSectionDimensions.y, generationParameters.MetersPerInterval);

            localCenter = GetPointInValidArea();
            transform.localPosition = new Vector3(
                localCenter.x * logicalLocalScale.x,
                localCenter.y * logicalLocalScale.y,
                transform.localPosition.z);
        }

        private Vector2 GetPointInValidArea()
        {
            float halfWidth = innerRectangleDimensionProportions.x * 0.5f,
                halfHeight = innerRectangleDimensionProportions.y * 0.5f;
            float x = Random.Range(-halfWidth, halfWidth),
                y = Random.Range(-halfHeight, halfHeight);
            return new Vector2(x, y);
        }

        public virtual void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Player")
            {
                DealDamage();
            }
        }

        protected void OnDrawGizmosSelected()
        {
#if UNITY_EDITOR
            Vector3 bottomLeft = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(-innerRectangleDimensionProportions.x * 0.5f, -innerRectangleDimensionProportions.y * 0.5f, 0)),
                bottomRight = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(innerRectangleDimensionProportions.x * 0.5f, -innerRectangleDimensionProportions.y * 0.5f, 0)),
                topLeft = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(-innerRectangleDimensionProportions.x * 0.5f, innerRectangleDimensionProportions.y * 0.5f, 0)),
                topRight = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(innerRectangleDimensionProportions.x * 0.5f, innerRectangleDimensionProportions.y * 0.5f, 0));

            Gizmos.color = wireColor;
            Gizmos.DrawLine(bottomLeft, topLeft);
            Gizmos.DrawLine(topLeft, topRight);
            Gizmos.DrawLine(topRight, bottomRight);
            Gizmos.DrawLine(bottomRight, bottomLeft);
#endif
        }
    }
}
