﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MusicGeneration;
using PizanaUtility;

public class DangersManager : MonoBehaviour
{
    public static DangersManager instance;

    public float samplingPeriodPerBeat = 0.2f,
        totalSampledBeats = 4f;//Sampled seconds has to be greater than the greater sampling delay of any drone

    //[Header("Goalkeeper Drone Parameters")]
    //public float followDelay = 2f;
    //public float movementPeriod = 3f;
    //public float movementSpeed = 10f;
    //public float movementDuration = 0.5f;

    //Vector2 lastSampledPosition;
    CircularBuffer<Vector2> lastSampledPositions;

    CharacterManager charManager;
    //Vector3[] characterPositions;
    //Queue<Vector3> positionsQueue;

    float samplingTimer = 0f;
    [HideInInspector]
    public float secondsPerBeat = -1f;

    [HideInInspector]
    public Transform markersParent;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;

        //characterPositions = new Vector3[Mathf.FloorToInt(totalSampledSeconds/samplingPeriod)];
        //positionsQueue = new Queue<Vector3>(Mathf.FloorToInt(totalSampledSeconds / samplingPeriod));
        samplingTimer = 0f;
        markersParent = new GameObject("Markers Parent").transform;
        markersParent.parent = transform;
    }

    public void Initialize()
    {
        charManager = CharacterManager.instance;
        secondsPerBeat = MusicManager.instance.Song.SecondsPerBeat();
        lastSampledPositions = new CircularBuffer<Vector2>(Mathf.CeilToInt(secondsPerBeat * (totalSampledBeats / samplingPeriodPerBeat)), Vector2.zero);
    }

    //private void Update()
    //{
    //    samplingTimer += Time.deltaTime;

    //    if (samplingTimer > samplingPeriodPerBeat)
    //    {
    //        SampleCharacterPosition();
    //        samplingTimer = 0f;
    //    }
    //}

    //public void SampleCharacterPosition()
    //{
    //    Vector2 lastSampledPosition;
    //    Vector2 globalPosition = CharacterManager.instance.virtualPosition;

    //    //We use the vector as placeholder
    //    lastSampledPosition = parentTrackTile.transform.worldToLocalMatrix * globalPosition;
    //    lastSampledPosition = new Vector2(lastSampledPosition.x / logicalLocalScale.x,
    //        lastSampledPosition.y / logicalLocalScale.y);
    //    lastSampledPositions.Enqueue(charManager.virtualPosition);
    //}

    //public Vector2 GetLocalTilePositionInTime(float secondsAgo)
    //{
    //    float continuousIndexValue = secondsAgo / (secondsPerBeat * samplingPeriodPerBeat);

    //    int floorIndex = Mathf.FloorToInt(continuousIndexValue),
    //        ceilIndex = Mathf.CeilToInt(continuousIndexValue);

    //    float remain = continuousIndexValue - ceilIndex;

    //    if (ceilIndex >= lastSampledPositions.Count) ceilIndex = floorIndex;

    //    return Vector2.Lerp(lastSampledPositions[floorIndex], lastSampledPositions[ceilIndex], remain);
    //}
}


