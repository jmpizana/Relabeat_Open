﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: 
 * a danger could be a "silence zone"-- an interval where you can do nothing
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace TrackGeneration
{

    [System.Serializable]
    public abstract class TrackDanger : MonoBehaviour
    {
        [Header("General Danger Parameters")]
        public TrackDangerParameters parameters;

        [HideInInspector]
        public TrackTile parentTrackTile;

        public abstract void Initialize(TrackTile tile, TrackGenerationProperties generationParameters);
        //public abstract void Update(float deltaTime);

        protected void PlaceAtStartOfTile(TrackGenerationProperties generationProperties)
        {
            transform.localPosition = new Vector3(0, 0, -0.5f * generationProperties.MetersPerInterval);
        }

        protected void DealDamage()
        {
            CharacterManager player = CharacterManager.instance;

            if (player.TakeDamage())
            {
                SoundEffectsManager.instance.PlaySound(
                    SoundEffectsManager.instance.dangerClipsDictionary[parameters.type].GetRandom(), 
                    false, 1f + Random.Range(-1f, 1f) * .1f);
            }
        }

        /// <summary>
        /// Returns positive if the danger is in the future and negative if the character is in the future
        /// </summary>
        /// <returns></returns>
        public float TimeToCharacter()
        {
            float timeOfTile = parentTrackTile.parameters.secondInSong + SecondsBeforeSongStart;

            float timeOfCharacter = Time.time - GameManager.instance.playStartTime;

            //Debug.Log(timeOfTile + " " + timeOfCharacter);

            return timeOfTile - timeOfCharacter;
        }

        private float secondsBeforeSongStart = -1f;
        private float SecondsBeforeSongStart
        {
            get
            {
                if (secondsBeforeSongStart < 0)
                    secondsBeforeSongStart = GameManager.instance.parameters.properties.secondsBeforeSongStart;

                return secondsBeforeSongStart;
            }
        }

        /*protected virtual void Update()
        {
            if (transform.position.z < -5f) Destroy(gameObject);//TODO: Destroy de optimización chapucera, cambiar para el futuro
        }*/

        protected Vector2 bottomLeftLocal = new Vector2(-.5f, -.5f),
        bottomRightLocal = new Vector2(.5f, -.5f),
        upperLeftLocal = new Vector2(-.5f, .5f),
        upperRightLocal = new Vector2(.5f, .5f);//TODO: move to constants
    }

    [System.Serializable]
    public struct TrackDangerParameters
    {
        public int difficultyValue;

        [SearchableEnum] public TrackDangerType type;

        public int tileDuration;

        public TrackDangerParameters(
            //float secondInSong, 
            TrackDangerParameters templateData, int tileDuration)
        {
            difficultyValue = templateData.difficultyValue;
            type = templateData.type;
            //secondInsong = secondInSong;
            this.tileDuration = tileDuration;
        }
    }

    

}
