﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * Laser placement is done by setting its central point and then a rotation
*/
using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using PizanaUtility;

namespace TrackGeneration
{
    public abstract class LaserDanger : TrackDanger
    {
        //public Vector3 start, end;
        [Header("General Laser Parameters")]
        protected Vector2 localCenter;
        protected float localAngle;
        public Color laserColor = Color.red;

        [Tooltip("Rectangle centered on the game track where the center point of the laser has to fall in order to be considered correct.")]
        public Vector2 innerRectangleDimensionProportions;

        [Header("Audiovisual")]
        public GameObject emitterPrefab;
        public LineRenderer laserLinePrefab;
        public BoxCollider colliderPrefab;

        protected GameObject emitter1, emitter2;
        protected LineRenderer laserLine;
        protected BoxCollider boxCollider;

        Vector3 logicalLocalScale;

        [Header("Editor Debug Parameters")]
        public Color wireColor = Color.white;

        public override void Initialize(TrackTile tile, TrackGenerationProperties generationParameters)
        {
            PlaceAtStartOfTile(generationParameters);

            //Place danger just on the vertical - horizontal center of the tile            
            logicalLocalScale = new Vector3(tile.tileSectionDimensions.x, tile.tileSectionDimensions.y, generationParameters.MetersPerInterval);

            localCenter = GetPointInValidArea();
            localAngle = Random.Range(0f, 180f);

            emitter1 = Instantiate(emitterPrefab, transform, true);
            emitter2 = Instantiate(emitterPrefab, transform, true);

            SetEmittersPosition();

            laserLine = Instantiate(laserLinePrefab, transform);
            boxCollider = Instantiate(colliderPrefab, transform);
            SetLaserLinePositions();
            SetColliderPositions();
        }

        protected void SetColliderPositions()
        {
            Vector3 pos1 = emitter1.transform.position,
                pos2 = emitter2.transform.position;
            Vector3 dist = pos2 - pos1;

            boxCollider.size = new Vector3(dist.magnitude, laserLine.widthMultiplier, laserLine.widthMultiplier);
            
            float angle = Vector2.SignedAngle(Vector2.right, dist);

            boxCollider.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }

        protected void SetLaserLinePositions()
        {
            //laserLine.transform.localPosition = localCenter;
            //laserLine.transform.localRotation = Quaternion.Euler(0f, 0f, localAngle);

            Vector3[] laserPositions = new Vector3[laserLine.positionCount];
            laserLine.GetPositions(laserPositions);

            for (int i = 0; i < laserPositions.Length; i++)
            {
                float normalizedValue = (float)i / ((float)laserPositions.Length - 1f);

                laserPositions[i] = Vector3.Lerp(emitter1.transform.localPosition, emitter2.transform.localPosition, normalizedValue);
            }

            laserLine.SetPositions(laserPositions);
        }

        protected void SetEmittersPosition()
        {
            //This needs for local angle and center to be previously set


            Vector2 emitter1LocalPos, emitter2LocalPos;
            emitter1LocalPos = FindEmitterPosition(false);
            emitter2LocalPos = FindEmitterPosition(true);

            emitter1.transform.localPosition = new Vector2(emitter1LocalPos.x * logicalLocalScale.x, emitter1LocalPos.y * logicalLocalScale.y);
            emitter2.transform.localPosition = new Vector2(emitter2LocalPos.x * logicalLocalScale.x, emitter2LocalPos.y * logicalLocalScale.y);
            emitter1.transform.LookAt(emitter2.transform.position);
            emitter2.transform.LookAt(emitter1.transform.position);
        }

        private Vector2 FindEmitterPosition(bool inverse)
        {
            Vector2 emitter1LocalPos;
            float longestDistanceInUnitRect = .707106f; //TODO: move to constants

            Vector2 overlapCirclePoint = longestDistanceInUnitRect * new Vector2(Mathf.Cos(localAngle + (inverse ? Mathf.PI : 0)), Mathf.Sin(localAngle + (inverse ? Mathf.PI : 0)));

            if (!MathHelper.SegmentsIntersection(Vector3.zero, overlapCirclePoint, bottomLeftLocal, upperLeftLocal, out emitter1LocalPos))
                if (!MathHelper.SegmentsIntersection(Vector3.zero, overlapCirclePoint, upperLeftLocal, upperRightLocal, out emitter1LocalPos))
                    if (!MathHelper.SegmentsIntersection(Vector3.zero, overlapCirclePoint, upperRightLocal, bottomRightLocal, out emitter1LocalPos))
                        MathHelper.SegmentsIntersection(Vector3.zero, overlapCirclePoint, bottomRightLocal, bottomLeftLocal, out emitter1LocalPos);

            return emitter1LocalPos;
        }

        private Vector2 GetPointInValidArea()
        {
            float halfWidth = innerRectangleDimensionProportions.x * 0.5f,
                halfHeight = innerRectangleDimensionProportions.y * 0.5f;
            float x = Random.Range(-halfWidth, halfWidth),
                y = Random.Range(-halfHeight, halfHeight);
            return new Vector2(x, y);
        }

        public virtual void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Player")
            {
                DealDamage();
            }
        }

        protected void OnDrawGizmosSelected()
        {
#if UNITY_EDITOR
            Vector3 bottomLeft = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(-innerRectangleDimensionProportions.x * 0.5f, -innerRectangleDimensionProportions.y * 0.5f, 0)),
                bottomRight = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(innerRectangleDimensionProportions.x * 0.5f, -innerRectangleDimensionProportions.y * 0.5f, 0)),
                topLeft = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(-innerRectangleDimensionProportions.x * 0.5f, innerRectangleDimensionProportions.y * 0.5f, 0)),
                topRight = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(innerRectangleDimensionProportions.x * 0.5f, innerRectangleDimensionProportions.y * 0.5f, 0));

            Gizmos.color = wireColor;
            Gizmos.DrawLine(bottomLeft, topLeft);
            Gizmos.DrawLine(topLeft, topRight);
            Gizmos.DrawLine(topRight, bottomRight);
            Gizmos.DrawLine(bottomRight, bottomLeft);
#endif
        }
    }
}


