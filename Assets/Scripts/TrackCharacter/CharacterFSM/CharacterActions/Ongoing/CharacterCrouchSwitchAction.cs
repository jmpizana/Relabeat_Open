﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Crouch Switch!")]
public class CharacterCrouchSwitchAction : CharacterOngoingAction
{
    public AnimationCurve switchStyle;
    public bool toCrouched = true;

    float initialHeight, targetHeight, currentHeight, newHeight, heightDiff;

    public override void Act(CharacterFSMController controller)
    {
        //TODO: externalize animation transition time from animator
        //controller.characterManager.animator.SetTrigger("Crouch");

        controller.crouchTransitionTimer += controller.deltaTime;

        float i = Mathf.Min(controller.crouchTransitionTimer / (toCrouched ? controller.crouchInSwitchTime : controller.crouchOffSwitchTime), 1f);

        currentHeight = controller.characterManager.capsCollider.height;

        if (toCrouched)
        {
            initialHeight = controller.standingHeight;
            targetHeight = controller.crouchedHeight;
        }
        else
        {
            initialHeight = controller.crouchedHeight;
            targetHeight = controller.standingHeight;
        }

        newHeight = Mathf.Lerp(initialHeight, targetHeight, switchStyle.Evaluate(i));
        heightDiff = newHeight - currentHeight;
        //Debug.Log("currentHeight: " + currentHeight + " || newHeight: " + newHeight + 
        //    "\nheightDiff: " + heightDiff);
        controller.characterManager.capsCollider.height = newHeight;
        controller.characterManager.virtualPosition += Vector3.up * heightDiff * 0.5f;
        //Debug.Log(controller.characterManager.virtualPosition);

        controller.characterManager.energy = Mathf.Max(0f,
                    controller.characterManager.energy - controller.characterManager.parameters.crouchCostPerSecond * controller.deltaTime
                    );

    }
}
