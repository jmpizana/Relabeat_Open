﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;
using TrackGeneration;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Head Bob!"), System.Serializable]
public class CharacterHeadBobAction : CharacterOngoingAction
{
    public float stepsPerBeat = 2f;

    public override void Act(CharacterFSMController controller)
    {
        CharacterManager charManager = CharacterManager.instance;
        float stepInterval = TrackManager.instance.properties.metersPerBeat / stepsPerBeat;

        charManager.cam.transform.localPosition =
             charManager.headBob.DoHeadBob(CharacterManager.instance.ForwardSpeedMag, stepInterval);
    }
}
