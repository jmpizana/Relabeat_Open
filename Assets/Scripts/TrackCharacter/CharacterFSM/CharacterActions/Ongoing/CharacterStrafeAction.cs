﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Strafe!"), System.Serializable]
public class CharacterStrafeAction : CharacterOngoingAction
{
    public float maxStrafeSpeed = 3f,
        strafeAccelerationMultiplier = 3f
        //strafeRecoveryAcceleration = 6f //Acceleration when speed increment is contrary to currentStrafeSpeed
        ;

    public AnimationCurve accelerationCurve;

    [Tooltip("How much the body resist to accelerate proportionally to speed."), Range(0f, 100f)]
    public float unpressedDrag = 0.5f, pressedDrag = 0.1f;//TODO: CHARACTER DRAG

    [Tooltip("How much the character opposes to acceleration")]
    public float strafeMass = 1f;

    public override void Act(CharacterFSMController controller)
    {
        Vector3 strafeAxis, currentStrafeSpeed, speedIncrement, dragVector;
        float newStrafeSpeedMag;

        strafeAxis = controller.characterManager.GetStrafeAxis.normalized;
        currentStrafeSpeed = Vector3.Project(controller.characterManager.virtualSpeed, strafeAxis);

        //Speed increment calculation
        {
            speedIncrement = strafeAxis * InputManager.instance.strafe.x * //How much the players presses to the sides
                  controller.deltaTime
                 / (strafeMass != 0f ? 1f / strafeMass : 1f) //Mass effect jejejeje
                 ;

            //Acceleration curve determination
            float curveValue = speedIncrement.Sign(currentStrafeSpeed) * currentStrafeSpeed.magnitude / maxStrafeSpeed;
            //A value between -1 and 1
            float accel = accelerationCurve.Evaluate(curveValue) * strafeAccelerationMultiplier;
            speedIncrement *= accel;

            //If speedIncrement is contrary to current speed we use recovery acceleration
            //if (speedIncrement.Sign(currentStrafeSpeed) < 0)
            //    speedIncrement *= strafeRecoveryAcceleration;
            //else
            //    speedIncrement *= strafeNormalAcceleration;
        }

        Vector3 newStrafeSpeed = (currentStrafeSpeed + speedIncrement);
        newStrafeSpeedMag = newStrafeSpeed.magnitude;

        if (newStrafeSpeedMag > maxStrafeSpeed)
        {
            //Debug.Log("Reached Max speed");
            //We use the unitary speed increment to keep the sign
            speedIncrement = newStrafeSpeed.normalized * (maxStrafeSpeed - currentStrafeSpeed.magnitude);
            newStrafeSpeedMag = maxStrafeSpeed;
        }

        float drag;
        if (speedIncrement != Vector3.zero) drag = pressedDrag;

        else drag = unpressedDrag;

        dragVector = strafeAxis * -newStrafeSpeed.Sign(strafeAxis) * drag * controller.deltaTime;

        if (dragVector.magnitude > newStrafeSpeedMag)
        
            dragVector = dragVector.normalized * newStrafeSpeedMag;
        

        controller.characterManager.virtualSpeed += speedIncrement + dragVector;

        //Debug.Log("New Strafe Speed: " + newStrafeSpeed + " || Current Strafe Speed: " + currentStrafeSpeed.ToString("F4") +
        //    "\nSpeed Increment: " + speedIncrement.ToString("F4") + " || Drag Vector: " + dragVector.ToString("F4") +
        //    "\n"
        //    );
    }


}
