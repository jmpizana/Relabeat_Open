﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * Switches camera tilts and sets steps for all states
 * Done like that because it needs to be inter-state
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Camera Manipulation!"), System.Serializable]
public class CharacterCameraManipulationAction : CharacterOngoingAction
{
    public override void Act(CharacterFSMController controller)
    {
        //TODO
        //throw new System.NotImplementedException();
        controller.characterManager.LookCamera();
    }
}
