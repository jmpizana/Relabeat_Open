﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: add cooldown to dash
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Dash!")]
public class CharacterDashAction : CharacterOngoingAction
{
    public float dashSpeed = 10f;

    public override void Act(CharacterFSMController controller)
    {
        //Debug.Log("Character Dash Action acting");
        controller.dashTimer += controller.deltaTime;

        Vector3 strafeAxis = controller.characterManager.GetStrafeAxis,
            previousVirtualSpeed = controller.characterManager.virtualSpeed,
            previousLateralSpeedComponent = Vector3.Project(previousVirtualSpeed, strafeAxis),
            newVirtualSpeed = previousVirtualSpeed - previousLateralSpeedComponent + strafeAxis * dashSpeed * controller.dashDirection;

        //Debug.Log("Strafe Axis: " + strafeAxis + " || Previous Virtual Speed: " + previousVirtualSpeed + 
        //    "\nPrevious Lateral Speed Component: " + previousLateralSpeedComponent + " || New Virtual Speed: " + newVirtualSpeed);
        
        controller.characterManager.virtualSpeed = newVirtualSpeed;
    }
}
