﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Instant!/Restart Head Bob!"), System.Serializable]
public class CharacterRestartHeadBobAction : CharacterInstantAction
{

    public override void Act(CharacterFSMController controller)
    {
        CharacterManager.instance.cam.transform.localPosition = 
            CharacterManager.instance.originalCamLocalPos;
    }
}
