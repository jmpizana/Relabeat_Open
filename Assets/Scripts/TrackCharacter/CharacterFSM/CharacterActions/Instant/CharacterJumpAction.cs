﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Instant!/Jump!"), System.Serializable]
public class CharacterJumpAction : CharacterInstantAction
{
    public override void Act(CharacterFSMController controller)
    {
        //TODO: care, this does only work when up direction is the canonical one
        controller.characterManager.virtualSpeed += controller.characterManager.parameters.jumpStrength * Vector3.up;
    }
}
