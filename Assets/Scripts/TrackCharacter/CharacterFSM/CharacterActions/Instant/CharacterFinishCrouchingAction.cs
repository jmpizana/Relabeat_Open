﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Instant!/Finish Crouching!")]
public class CharacterFinishCrouchingAction : CharacterInstantAction
{
    public bool toCrouched = true;
    public bool correctHeight = false;
    float targetHeight;
    public override void Act(CharacterFSMController controller)
    {
        float currentHeight = controller.characterManager.capsCollider.height;

        targetHeight = toCrouched ? controller.crouchedHeight : controller.standingHeight;
        controller.crouchTransitionTimer = 0f;
        controller.characterManager.capsCollider.height = targetHeight;

        if (correctHeight)
        {
            float heightDiff = targetHeight - currentHeight;
            controller.characterManager.virtualPosition += Vector3.up * heightDiff * 0.5f;
        }

        
    }
}
