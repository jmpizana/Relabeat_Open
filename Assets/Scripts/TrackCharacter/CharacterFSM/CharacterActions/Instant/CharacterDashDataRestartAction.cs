﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Instant!/Dash Data Restart!"), System.Serializable]
public class CharacterDashDataRestartAction : CharacterInstantAction
{
    public override void Act(CharacterFSMController controller)
    {
        controller.dashTimer = 0f;
        controller.dashDirection = 0;
    }
}
