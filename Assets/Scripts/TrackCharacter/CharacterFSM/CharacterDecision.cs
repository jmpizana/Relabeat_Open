﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility.FSM;

public abstract class CharacterDecision : FSMDecision
    <CharacterFSMController, CharacterState, CharacterOngoingAction, CharacterInstantAction, CharacterTransition, CharacterDecision>
{

}
