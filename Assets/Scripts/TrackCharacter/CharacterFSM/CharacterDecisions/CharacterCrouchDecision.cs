﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Crouch?"), System.Serializable]
public class CharacterCrouchDecision : CharacterDecision
{
    public bool requireEnergy = false;

    protected override bool Assert(CharacterFSMController controller)
    {
        bool value = InputManager.instance.crouch;

        if (requireEnergy)
            value = value && controller.characterManager.energy >= controller.characterManager.parameters.crouchCostPerSecond;

        return value;
    }

}
