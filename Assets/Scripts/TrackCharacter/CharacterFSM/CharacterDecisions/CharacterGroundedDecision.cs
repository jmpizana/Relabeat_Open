﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Grounded?"), System.Serializable]
public class CharacterGroundedDecision : CharacterDecision
{
    protected override bool Assert(CharacterFSMController controller)
    {
        Vector3 landPoint;
        //Debug.Log(controller.characterManager.CheckIfGrounded() ? "Estamos Grounde":"No estamos Grounde");
        bool result =  controller.characterManager.CheckIfGrounded(out landPoint);

        controller.landPoint = landPoint;

        return result;
    }
}
