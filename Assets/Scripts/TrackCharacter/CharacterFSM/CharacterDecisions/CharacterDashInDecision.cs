﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Dash In?"), System.Serializable]
public class CharacterDashInDecision : CharacterDecision
{
    protected override bool Assert(CharacterFSMController controller)
    {
        //Vector2 strafe = InputManager.instance.strafe;
        //if (InputManager.instance.dash && strafe.x != 0)
        float dash = InputManager.instance.dash;
        if (dash!=0 && 
            controller.characterManager.energy >= controller.characterManager.parameters.dashCost)
        {
            controller.dashDirection = (int)Mathf.Sign(dash);
            controller.dashTimer = 0f;

            controller.characterManager.energy -= controller.characterManager.parameters.dashCost;
            return true;
        }

        return false;

    }
}
