﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Finish Crouching Transition?"), System.Serializable]
public class CharacterFinishCrouchingTransitionDecision : CharacterDecision
{
    public bool crouchIn = true;

    protected override bool Assert(CharacterFSMController controller)
    {
        return controller.crouchTransitionTimer >= (crouchIn ? controller.crouchInSwitchTime : controller.crouchOffSwitchTime);
    }
}
