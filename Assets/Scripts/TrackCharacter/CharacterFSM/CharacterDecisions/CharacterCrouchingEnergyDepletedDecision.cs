﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Crouching Energy Depleted?"), System.Serializable]
public class CharacterCrouchingEnergyDepletedDecision : CharacterDecision
{
    protected override bool Assert(CharacterFSMController controller)
    {
        return 
            controller.characterManager.energy < 
            controller.characterManager.parameters.crouchCostPerSecond * controller.deltaTime;
    }
}
