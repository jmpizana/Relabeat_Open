﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Jump?"), System.Serializable]
public class CharacterJumpDecision : CharacterDecision
{
    protected override bool Assert(CharacterFSMController controller)
    {
        bool result =  InputManager.instance.jump && //TODO: maybe this needs to be changed to jumping in the future
            controller.characterManager.energy >= controller.characterManager.parameters.jumpCost;

        if(result)
            controller.characterManager.energy -= controller.characterManager.parameters.jumpCost;

        return result;
    }
}
