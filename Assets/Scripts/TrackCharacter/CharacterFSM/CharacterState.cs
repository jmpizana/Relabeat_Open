﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility.FSM;

[CreateAssetMenu(menuName = "AI/Character FSM/State"), System.Serializable]
public class CharacterState : FSMState
    <CharacterFSMController, CharacterState, CharacterOngoingAction, CharacterInstantAction, CharacterTransition, CharacterDecision>
{

}
