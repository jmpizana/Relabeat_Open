﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility.FSM;

public abstract class CharacterOngoingAction : FSMOngoingAction
    <CharacterFSMController, CharacterState, CharacterOngoingAction, CharacterInstantAction, CharacterTransition, CharacterDecision>
{

}
