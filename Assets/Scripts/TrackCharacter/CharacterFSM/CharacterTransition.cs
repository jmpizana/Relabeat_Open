﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility.FSM;

//[CreateAssetMenu(menuName = "AI/Character FSM/Transition"), System.Serializable]
[System.Serializable]
public class CharacterTransition : FSMTransition
    <CharacterFSMController, CharacterState, CharacterOngoingAction, CharacterInstantAction, CharacterTransition, CharacterDecision>
{ 

	
}
