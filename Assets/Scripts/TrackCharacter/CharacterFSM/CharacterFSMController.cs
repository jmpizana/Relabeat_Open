﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility.FSM;

[RequireComponent(typeof(CharacterManager)), System.Serializable]
public class CharacterFSMController : FSMController
    <CharacterFSMController, CharacterState, CharacterOngoingAction, CharacterInstantAction, CharacterTransition, CharacterDecision>
{
    public float crouchedHeight = .5f;
    public float crouchInSwitchTime = 0.25f, crouchOffSwitchTime = 0.25f;


    [HideInInspector] public CharacterManager characterManager;
    [HideInInspector] public float deltaTime;


    [HideInInspector] public int dashDirection = 0;
    [HideInInspector] public float dashTimer = 0f;

    [HideInInspector] public float crouchTransitionTimer = 0f, standingHeight;

    public Vector3 landPoint = Vector3.zero;
    public override void Awake()
    {
        base.Awake();
        characterManager = GetComponent<CharacterManager>();

    }

    private void Start()
    {
        standingHeight = characterManager.capsCollider.height;
    }

    public override void Update()
    {
        deltaTime = Time.deltaTime;

        base.Update();


    }

}
