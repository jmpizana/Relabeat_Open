﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * COMMENTS:
 * - Wall Climb removed as it does not fit the new game style
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMovementHelper : MonoBehaviour
{
    public enum WallMovementState { NONE, RUNNING/*, CLIMBING */}

    public LayerMask wallRuneableMasks;
    [Range(0, 1)] public float minWallRunSpeedPerc = 0.5f;
    public float wallRunTiltAngle = 5f, wallRunTiltSpeed = 0.5f, minIncomingAngleAdmitted = 45f, maxIncomingAngleAdmitted = 135f;
    public PlayerMovement parent;

    [HideInInspector] public RaycastHit wallHitInfo;

    private bool wall2Right = false, touchedFloor = false;
    public WallMovementState state = WallMovementState.NONE;
    private Collider currentWall, lastWall;//YOU CAN ONLY WALLRUN TO 1 OBJECT SIMULTANEOUSLY

    private void Awake()
    {
        minWallRunSpeedPerc = Mathf.Clamp01(minWallRunSpeedPerc);
    }

    private void OnTriggerStay(Collider other)
    {
        if (parent.IsGrounded || other == lastWall) return;

        //If the other object is in a wallRuneable mask
        if (wallRuneableMasks == (wallRuneableMasks | (1 << other.gameObject.layer)))        
            currentWall = other;
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == currentWall)
        {
            lastWall = currentWall;
            currentWall = null;
        }
    }

    public void WallMovementCheck()
    {
        //Debug.Log("parent.speed.magnitude < parent.currentTargetSpeed * minWallRunSpeedPerc");
        if (currentWall == null || parent.activeSpeed.magnitude < parent.targetSpeed * minWallRunSpeedPerc)
        {
            SetWallMovement(WallMovementState.NONE);
            return;
        }

        Vector3 distance = currentWall.transform.position - transform.parent.position;

        if (Physics.Raycast(transform.parent.position, distance, out wallHitInfo, distance.magnitude * 1.5f, wallRuneableMasks))
        {
            float signedAngle = Vector3.SignedAngle(transform.parent.forward, wallHitInfo.normal, Vector3.up);
            //Debug.Log(signedAngle);
            float angle = Mathf.Abs(signedAngle);
            //Debug.Log(angle);
            if (angle > minIncomingAngleAdmitted && angle < maxIncomingAngleAdmitted)
            {
                //Debug.Log("Wallrunning");
                if (signedAngle < 0) wall2Right = true;
                else wall2Right = false;

                SetWallMovement(WallMovementState.RUNNING);
                return;
            }
            /*else if (angle >= maxIncomingAngleAdmitted)
            {
                SetWallMovement(WallMovementState.CLIMBING);
                return;
            }*/
        }
        else
        {
            Debug.Log("Raycast failed");
            SetWallMovement(WallMovementState.NONE);
        }
    }

    private void SetWallMovement(WallMovementState newState)
    {
        //parent.rB.useGravity = !value;
        //wallRunning = value;
        state = newState;
    }

    public float WallRunAngleCorrection()
    {
        if (state == WallMovementState.RUNNING) return (wall2Right ? 1f : -1f) * wallRunTiltAngle;

        else return 0f;

    }

    public bool IsWallRunning { get { return state == WallMovementState.RUNNING; } }
    //public bool IsWallClimbing { get { return state == WallMovementState.CLIMBING; } }

    public void OnLand()
    {
        lastWall = null;
    }
}
