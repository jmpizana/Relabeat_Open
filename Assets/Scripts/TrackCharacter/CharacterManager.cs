﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrackGeneration;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Rendering.PostProcessing;
using UnityStandardAssets.Utility;

[RequireComponent(typeof(CharacterFSMController))]
public class CharacterManager : MonoBehaviour
{
    public static CharacterManager instance;

    [Header("Parameters")]
    public CharacterParameters parameters;
    public MouseLook mouseLook;
    public CurveControlledBob headBob;

    [Header("References")] public WallMovementHelper wallMovement;

    [Header("DEBUG")] public bool drawDebugs = true;

    #region Game Parameters
    public int health;
    public float energy, time;
    public bool invulnerable = false;
    #endregion

    //Private parameters
    TrackManager trackManager;
    [HideInInspector] public Camera cam;
    [HideInInspector] public Vector3 originalCamLocalPos;
    InputManager inputManager;
    EffectsManager fxManager;
    CharacterFSMController charFSM;

    //[HideInInspector] public Animator animator;
    [HideInInspector] public CapsuleCollider capsCollider;
    public Vector3 virtualSpeed = Vector3.zero;
    public Vector3 virtualPosition = Vector3.zero;
    bool grounded;
    bool groundable = true;
    float invulnTimeStart = 0f;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;
    }


    public void Initialize()
    {
        trackManager = TrackManager.instance;
        inputManager = InputManager.instance;
        fxManager = EffectsManager.instance;
        cam = Camera.main;
        originalCamLocalPos = cam.transform.localPosition;
        capsCollider = GetComponent<CapsuleCollider>();
        charFSM = GetComponent<CharacterFSMController>();
        //animator = GetComponent<Animator>();
        headBob.Setup(cam);
        health = parameters.maxHealth;
        energy = parameters.maxEnergy;
        time = 0f;

        mouseLook.Init(transform, cam.transform);


        virtualPosition = Vector3.zero;

        virtualSpeed = trackManager.properties.trackDirection.normalized * ForwardSpeedMag;
    }

    private float forwardSpeedMag = -1f;

    public float ForwardSpeedMag
    {
        get
        {
            if (forwardSpeedMag < 0f)
                forwardSpeedMag = trackManager.properties.seedSong.GetBPM() * trackManager.properties.MetersPerInterval * trackManager.properties.intervalsPerBeat / 60f;

            return forwardSpeedMag;
        }
    }

    //private void FixedUpdate()
    //{
    /*Character movement:
     * 
     * IMPORTANT NOTE: ACTUALLY IT IS THE ENVIRONMENT THAT SHOULD MOVE MIRRORING PLAYER'S PARAMETERS
     * 
     * 1.- Always moves at the direction of the track;
     * 2.- Can move the camera around in a 180º horizontal span and 180º vertical span
     * 3.- Can execute different skills based on state
     * 4.- Speed is intended to reach constant "speed" value soon, as music playback speed depends on it
     */

    //LookCamera();
    //ExecuteSkills();
    //}

    private void Update()
    {
        time += Time.deltaTime;

        if (invulnerable && time - invulnTimeStart >= parameters.invulnerabilityTime)
        {
            invulnerable = false;
            invulnTimeStart = float.NegativeInfinity;
        }

        RegenerateEnergy();
    }

    private void RegenerateEnergy()
    {
        float regenValue = parameters.energyRegenCurve.Evaluate((float)health / (float)parameters.maxHealth) * parameters.baseEnergyRegenPerSecond;
        energy = Mathf.Min(parameters.maxEnergy, energy + regenValue * Time.deltaTime);
    }

    public bool TakeDamage()
    {
        if (invulnerable) return false;

        health--;
        invulnerable = true;
        invulnTimeStart = time;//TODO: add visual effects to invulnerability

        EffectsManager.instance.DamagedEffect(parameters.invulnerabilityTime);

        if (health <= 0)
        {
            //Game is lost
            GameManager.instance.EndGameLoss();
        }

        return true;
    }

    public bool CheckIfGrounded(out Vector3 groundPoint)
    {
        groundPoint = Vector3.zero;
        if (!groundable) return false;

        RaycastHit hitInfo;

        float radius = parameters.rayRadius;
        float height = capsCollider.height;
        float startPointDistance = parameters.groundedRaycastStart * height;
        float endPointDistance = parameters.groundedRaycastEnd * height + height * parameters.groundedDistanceBufferPercentage;

        float pointsDistance = endPointDistance - startPointDistance;
        float rayDistance = pointsDistance - radius * 2f;

        //if (pointsDistance < 0) throw new UnityException("Points of character grounding raycast are inverted.");

#if UNITY_EDITOR
        if (drawDebugs)
        {
            Debug.DrawLine(transform.position + Vector3.down * startPointDistance - Vector3.right, transform.position + Vector3.down * startPointDistance + Vector3.right, Color.blue);
            Debug.DrawLine(transform.position + Vector3.down * endPointDistance - Vector3.right * .5f, transform.position + Vector3.down * endPointDistance + Vector3.right * .5f, Color.magenta);
            Debug.DrawLine(transform.position + Vector3.down * startPointDistance, transform.position + Vector3.down * endPointDistance, Color.white);
            Debug.DrawLine(transform.position + Vector3.down * startPointDistance, transform.position + Vector3.down * (pointsDistance + startPointDistance), Color.red);
            //DebugExtension.DebugCapsule(transform.position + Vector3.down * (startPointDistance - radius), transform.position + Vector3.down * (endPointDistance + radius), Color.red, parameters.rayRadius);
        }
#endif
        /*
        if (Physics.SphereCast(transform.position + Vector3.down * startPointDistance, radius,
                   Vector3.down, out hitInfo, pointsDistance, parameters.collidableLayers))
        {
            DebugExtension.DebugWireSphere(hitInfo.point, radius, 1);
            groundPoint = hitInfo.point;
            grounded = true;
        }
        else grounded = false;*/



        if (rayDistance <= 0)
        {

#if UNITY_EDITOR
            if (drawDebugs) DebugExtension.DebugWireSphere(transform.position + Vector3.down * (startPointDistance + pointsDistance * 0.5f), radius - (radius - pointsDistance * 0.5f));
#endif

            Collider[] colls = Physics.OverlapSphere(transform.position + Vector3.down * (startPointDistance + pointsDistance * 0.5f), radius - (radius - pointsDistance * 0.5f), parameters.collidableLayers);

            if (colls != null && colls.Length > 0)
            {
                Vector3 bottomPoint = transform.position + Vector3.down * height * 0.5f;
                groundPoint = colls[0].ClosestPointOnBounds(bottomPoint);

                float previousDistance = (bottomPoint - groundPoint).magnitude,
                    currentDistance;
                Vector3 currentPoint;

                for (int i = 1; i < colls.Length; i++)
                {
                    currentPoint = colls[i].ClosestPointOnBounds(bottomPoint);
                    currentDistance = (bottomPoint - currentPoint).magnitude;
                    if (currentDistance < previousDistance)
                    {
                        groundPoint = currentPoint;
                        previousDistance = currentDistance;//TODO: test this part
                    }
                }

#if UNITY_EDITOR
                if (drawDebugs) DebugExtension.DebugWireSphere(groundPoint, Color.green, .1f, 1f);
#endif

                grounded = true;
            }
            else grounded = false;

        }
        else
        {
            //Debug.Log(endPointDistance - startPointDistance - collider.radius);
            if (Physics.SphereCast(transform.position + Vector3.down * (startPointDistance + radius), radius,
                    Vector3.down, out hitInfo, rayDistance, parameters.collidableLayers))
            {
                groundPoint = hitInfo.point;
                grounded = true;
            }
            else grounded = false;
        }
        return grounded;
    }


    private void ExecuteSkills()
    {
        throw new NotImplementedException();

        /*Check for input (inputmanager)
         * Apply necessary changes / state changes
         
         */
    }

    public Vector3 GetGravityDirection
    {
        get
        {
            //TODO: change accordingly for all axes
            return Vector3.up;
        }
    }

    public Vector3 GetStrafeAxis
    {
        get
        {
            //TODO: do properly for all axes
            return Vector3.right;
        }

    }

    public CharacterFSMController FSMController
    {
        get { return charFSM; }
    }

    public float Height
    {
        get
        {
            return capsCollider.height;
        }
    }

    public void MakeNotGroundable(float seconds)
    {
        groundable = false;

        Invoke(nameof(UndoNotGroundable), seconds);
    }

    private void UndoNotGroundable()
    {
        groundable = true;
    }

    public void LookCamera()
    {
        mouseLook.LookRotation(transform, cam.transform);
    }
}
