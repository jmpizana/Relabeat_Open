﻿Shader "RelativisticTest"
{
    Properties
    {
		KFactor("K Value", float) = 1.0
		Scale("Scale", float) = 1.0
		ZValue("Z value", float) = 1.0
        [NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Pass
        {
            Tags {"LightMode"="ForwardBase"}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            // compile shader into multiple variants, with and without shadows
            // (we don't care about any lightmaps yet, so skip these variants)
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            // shadow helper functions and macros
            #include "AutoLight.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                SHADOW_COORDS(1) // put shadows data into TEXCOORD1
                fixed3 diff : COLOR0;
                fixed3 ambient : COLOR1;
                float4 pos : SV_POSITION;
            };

			uniform float KFactor;
			uniform float Scale;
			uniform float ZValue;

            v2f vert (appdata_base v)
            {
               
			    v2f o;
			   /*
				float4 mvPos = mul(UNITY_MATRIX_MV, v.vertex);
				float2 posProj = mvPos.xy;
				float r = length(posProj);
				float4 pos2 = float4
				(mvPos.x * (abs(mvPos.x) / (r + KFactor/Scale)), 
				 mvPos.y *  (abs(mvPos.y) / (r + KFactor/Scale)), 
				 mvPos.z *  (abs(mvPos.z) / (mvPos.z + KFactor/Scale)),
				1.0);
				o.pos =  mul(UNITY_MATRIX_P, pos2);*/
				/*
				float4 mvPos = mul(UNITY_MATRIX_MV, v.vertex);
				float2 posProj = mvPos.xy;
				float r = length(posProj);
				float4 pos2 = float4(mvPos.x / (r + KFactor/Scale), mvPos.y / (r + KFactor/Scale),  mvPos.z, 1.0);
				o.pos =  mul(UNITY_MATRIX_P, pos2);*/
				/*
				float4 mvPos = mul(UNITY_MATRIX_MV, v.vertex);
				float2 posProj = mvPos.xy;
				float r = length(posProj);
				float4 pos2 = float4(KFactor * posProj/(r+1),  mvPos.z * KFactor, 1.0);
				o.pos =  mul(UNITY_MATRIX_P, pos2);*/
				
				
				float4 pos = UnityObjectToClipPos(v.vertex);
				float2 posProj = pos.xy;
				float r = length(posProj);
				float4 pos2 = float4(posProj/(r+KFactor/Scale), pos.z, 1.0);
				o.pos = pos2;

				/*//Cartesian hyperbolic 
				float4 pos = UnityObjectToClipPos(v.vertex);
				float2 posProj = pos.xy;
				float r = length(posProj);
				float4 pos2 = float4(pos.x/sqrt(pos.x*pos.x + KFactor), pos.y/sqrt(pos.y*pos.y + KFactor), pos.z, 1.0);
				o.pos = pos2;*/

                //o.pos = UnityObjectToClipPos(v.vertex);

                o.uv = v.texcoord;
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0.rgb;
                o.ambient = ShadeSH9(half4(worldNormal,1));
                // compute shadows data
                TRANSFER_SHADOW(o)
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                // compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
                fixed shadow = SHADOW_ATTENUATION(i);
                // darken light's illumination with shadow, keep ambient intact
                fixed3 lighting = i.diff * shadow + i.ambient;
                col.rgb *= lighting;
                return col;
            }
            ENDCG
        }

        // shadow casting support
        UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}