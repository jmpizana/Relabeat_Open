﻿/*This script alters relative distances of objects to give a "relativistic" sensation.
 * I've implemented two different methods to test their effectivity, but preferably only per-vertex is desirable.
 */
 /*
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

public class RelativisticEnvironment : MonoBehaviour
{
    public MeshFilter[] staticEnvironment;
    public Vector3[][] originalVertexLists;
    private Vector3[] initialPositions, initialLocalScales;

    //CompShader parameters
    public ComputeShader vertexWarpCompShader;
    private int kernelIndex = 0, vertsAmount = 0, pw2Side = 0, side = 0;
    private Texture2D vertexPosTexture, originalVertexPosTexture;



    void Awake()
    {
        staticEnvironment = GetComponentsInChildren<MeshFilter>();
        //GetOriginalData();
    }

    private void Start()
    {
        GetOriginalData();
    }

    private void GetOriginalData()
    {
        //Gets all necessary pre-altered environment data

        switch (PhysicsManager.instance.relativityMethod)
        {
            case PhysicsManager.RelativityMethod.PER_OBJECT:
                initialPositions = new Vector3[staticEnvironment.Length];
                initialLocalScales = new Vector3[staticEnvironment.Length];
                for (int i = 0; i < staticEnvironment.Length; i++)
                {
                    initialPositions[i] = staticEnvironment[i].transform.position;
                    initialLocalScales[i] = staticEnvironment[i].transform.localScale;
                }

                break;

            case PhysicsManager.RelativityMethod.PER_VERTEX_COMPUTE_SHADER:
                InitializeGPUData();
                break;

            case PhysicsManager.RelativityMethod.PER_VERTEX:
                originalVertexLists = new Vector3[staticEnvironment.Length][];
                for (int i = 0; i < staticEnvironment.Length; i++)
                {
                    staticEnvironment[i].mesh.MarkDynamic();
                    originalVertexLists[i] = staticEnvironment[i].mesh.vertices;
                }
                break;
        }
    }
    /*
    unsafe private void InitializeGPUData()
    {
        /*Le falta bastante trabajo de bajo nivel. El problema es que c# no deja demasiado campo de manipulación con punteros,
         * por lo que intentar linkar directamente el resultado de la computación a cada respectivo vértice es bastante difícil.
         * Por ahora nos conformaremos con una solución subóptima
         *//*
        return;
        vertsAmount = 0;
        Vector3[][] verts = new Vector3[staticEnvironment.Length][];

        for (int i = 0; i < staticEnvironment.Length; i++)
        {
            verts[i] = staticEnvironment[i].mesh.vertices;
            vertsAmount += verts[i].Length;
        }

        side = (int)Mathf.Ceil(Mathf.Sqrt(vertsAmount));
        pw2Side = MathHelper.FindNearestBiggerPowerof2(side);

        kernelIndex = vertexWarpCompShader.FindKernel("RelativizePerVertex");
        originalVertexPosTexture = new Texture2D(pw2Side, pw2Side);//We "square" the data

        Color[] pixels = originalVertexPosTexture.GetPixels();

        int k = 0, obj = 0;
        while (k < vertsAmount)
        {
            for (int j = 0; j < verts[obj].Length; j++)
            {
                pixels[k % side + side * (k / side)] = (Vector4)verts[obj][j];
                k++;
            }
            obj++;
        }

        originalVertexPosTexture.SetPixels(pixels);

        vertexPosTexture = new Texture2D(pw2Side, pw2Side);//We "square" the data 
        Vector4*[] vectorPointers = new Vector4*[vertsAmount];
        Color[] vertexPositions = vertexPosTexture.GetPixels();

        for (int j = 0; j < vertsAmount; j++)
        {
            //vectorPointers[j] = (vertexPositions[j])* ;
        }
        //vertexPosTexture.enableRandomWrite = true;
    }
    *//*
    void Update()
    {
        if (PhysicsManager.instance.spaceControl)
        {
            Relativize();
        }
    }

    private void Relativize()
    {
        switch (PhysicsManager.instance.relativityMethod)
        {
            case PhysicsManager.RelativityMethod.PER_OBJECT:
                RelativizePerObject();
                break;

            case PhysicsManager.RelativityMethod.PER_VERTEX:
                RelativizePerVertex();
                break;

            case PhysicsManager.RelativityMethod.PER_VERTEX_COMPUTE_SHADER:
                RelativizePerVertexGPU();
                break;
        }
    }

    private void RelativizePerVertexGPU()
    {
        Vector3 speedRatios = PhysicsManager.instance.relativisticRatios;
        speedRatios = Vector3.one - speedRatios * PhysicsManager.instance.relativisticSpatialEffect;

        vertexWarpCompShader.SetTexture(kernelIndex, "Result", vertexPosTexture);

        vertexWarpCompShader.Dispatch(kernelIndex, pw2Side / 8, pw2Side / 8, 1);
    }

    private void RelativizePerObject()
    {


        for (int i = 0; i < initialPositions.Length; i++)
        {
            //We make the object a child of our reference system
            //childrenUnits[i].transform.parent = referenceSystem.transform;
            //childrenUnits[i].transform.localScale = Vector3.one + referenceSystem.rB.velocity.normalized * relativisticSpatialEffect / speedRatio;
            Vector3 unrotatedLocalScale = new Vector3(
                initialLocalScales[i].x / (1f + PhysicsManager.instance.relativisticRatios.x * PhysicsManager.instance.relativisticScaleEffect),
                initialLocalScales[i].y / (1f + PhysicsManager.instance.relativisticRatios.y * PhysicsManager.instance.relativisticScaleEffect),
                initialLocalScales[i].z / (1f + PhysicsManager.instance.relativisticRatios.z * PhysicsManager.instance.relativisticScaleEffect)
                );

            staticEnvironment[i].transform.localScale = staticEnvironment[i].transform.rotation * unrotatedLocalScale;

            Vector3 localDist = initialPositions[i] - PhysicsManager.instance.referencePoint;

            localDist = new Vector3(
                localDist.x * (1f - PhysicsManager.instance.relativisticRatios.x * PhysicsManager.instance.relativisticSpatialEffect),
                localDist.y * (1f - PhysicsManager.instance.relativisticRatios.y * PhysicsManager.instance.relativisticSpatialEffect),
                localDist.z * (1f - PhysicsManager.instance.relativisticRatios.z * PhysicsManager.instance.relativisticSpatialEffect)
                );

            staticEnvironment[i].transform.position = localDist + PhysicsManager.instance.referencePoint;


            //We return the object to its parent
            //childrenUnits[i].transform.parent = transform;

        }
    }

    private void RelativizePerVertex()
    {
        Vector3 speedRatios = PhysicsManager.instance.relativisticRatios;
        speedRatios = Vector3.one - speedRatios * PhysicsManager.instance.relativisticSpatialEffect;

        MeshFilter meshFilter;

        for (int i = 0; i < staticEnvironment.Length; i++)
        {
            meshFilter = staticEnvironment[i];

            //Vector3[] meshVertices = originalVertexLists[i];

            //Arrays are (obviously) passed by reference, soy we have to make a new array
            Vector3[] meshVertices = new Vector3[originalVertexLists[i].Length];
            for (int j = 0; j < meshVertices.Length; j++)
            {
                meshVertices[j] = originalVertexLists[i][j];
            }

            for (int j = 0; j < meshVertices.Length; j++)
            {
                Vector3 vertex = meshVertices[j];
                Vector3 distToReferenceOrigin = meshFilter.transform.TransformPoint(vertex) - PhysicsManager.instance.referencePoint;
                //Debug.DrawLine(meshFilter.transform.TransformPoint(vertex), Vector3.zero, Color.blue);
                distToReferenceOrigin = new Vector3(
                    distToReferenceOrigin.x * speedRatios.x,
                    distToReferenceOrigin.y /** speedRatios.y*//*,
                    distToReferenceOrigin.z * speedRatios.z
                    );


                //Debug.DrawLine(distToReferenceOrigin + referenceSystem.transform.position, Vector3.zero, Color.red);

                meshVertices[j] = meshFilter.transform.InverseTransformPoint(distToReferenceOrigin + PhysicsManager.instance.referencePoint);

            }

            meshFilter.mesh.vertices = meshVertices;
            meshFilter.mesh.RecalculateBounds();

            meshFilter.GetComponent<MeshCollider>().sharedMesh = meshFilter.mesh;
            //meshFilter.mesh.RecalculateNormals();

        }
    }
}*/