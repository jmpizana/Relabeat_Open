﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomDeathLaser : MonoBehaviour
{
    //public float offsetSpeed = 5f;

    Vector3 initialPosition;
    //MeshRenderer rend;
    //Vector2 offset = Vector2.zero;
    //Vector2 offsetDirection;

    private void Awake()
    {
        initialPosition = transform.position;
        //rend = GetComponent<MeshRenderer>();

        //offsetDirection = UnityEngine.Random.insideUnitCircle.normalized;
    }

    private void Update()
    {
        SetPositions();

        CheckIfPlayerDead();
    }

    private void CheckIfPlayerDead()
    {
        if (CharacterManager.instance.transform.position.y < transform.position.y)
        {
            GameManager.instance.EndGameLoss();
            enabled = false;
        }
    }

    private void SetPositions()
    {
        transform.position = new Vector3(
                    transform.position.x,
                    initialPosition.y - CharacterManager.instance.virtualPosition.y,
                    transform.position.z);

        //offset += offsetDirection * Time.deltaTime * offsetSpeed;

        //rend.material.mainTextureOffset = offset;
    }
}
