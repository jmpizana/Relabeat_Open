﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Parameters/Game Manager Parameters")]
public class GameManagerParameters : ScriptableObject
{
    public GameManagerProperties properties;
}

[System.Serializable]
public struct GameManagerProperties
{
    [Range(0f, 20f)]
    public float secondsBeforeSongStart;

    [Header("Prefabs")]
    public CharacterManager characterMovementPrefab;

 

}

