﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class InputManager : MonoBehaviour
{
    //TODO: maybe decoupling input from character's physics update is a bad idea

    public static InputManager instance;

    [Header("Input parameters"), HideInInspector] public bool jump;

    [HideInInspector] public bool jumping;

    [HideInInspector] public float dash;
    [HideInInspector] public float dashing;

    [HideInInspector] public bool crouch;
    [HideInInspector] public bool crouching;

    //[HideInInspector] public bool pause;

    [HideInInspector]
    public Vector2 strafe = Vector2.zero,
        mouseMovement = Vector2.zero;

    private float internalJumpThreshold = 0.8f;

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);

        instance = this;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {

        strafe = new Vector2
        (
             Input.GetAxis("Horizontal"),
             Input.GetAxis("Vertical")
        );

        if (Cursor.visible)
            mouseMovement = Vector2.zero;
        else
            mouseMovement = new Vector2(
                Input.GetAxis("Turn X"),
                Input.GetAxis("Turn Y"));

        //jump = Input.GetAxisRaw("Jump") > internalJumpThreshold;
        //if (jump) Debug.Log("jump");
        jump = Input.GetButtonDown("Jump");
        jumping = Input.GetButton("Jump");

        dash = Input.GetAxis("Dash");
        dashing = Input.GetAxis("Dash");

        crouch = Input.GetButtonDown("Crouch");
        crouching = Input.GetButton("Crouch");


        UpdateCursorBehavior();
    }

    

    private void UpdateCursorBehavior()
    {
        if (GameManager.instance.playing)
        {
            if (Cursor.lockState == CursorLockMode.None && Input.GetMouseButtonDown(0))
            {
                SetMouseLock(true);
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                SetMouseLock(false);
            }
        }
        else
        {
            if (Cursor.lockState != CursorLockMode.None)
                SetMouseLock(false);
        }
    }

    public void SetMouseLock(bool locked)
    {
        if (locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
