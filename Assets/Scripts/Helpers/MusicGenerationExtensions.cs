﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MusicGeneration
{
    public static class SongCollectionsExtensions
    {

        public static bool HasSegmentAudiosAlready(this Queue<SongSegmentSources> sourcesQueue,
            SongSegment segment)
        {
            SongSegmentSources[] array = sourcesQueue.ToArray();

            for (int i = 0; i < array.Length; i++)
                if (array[i].segment == segment)
                    return true;


            return false;
        }

    }

}
