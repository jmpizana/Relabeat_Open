﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundDrivenValueDampened
{
    public bool active = false;
    public float baseIntensityProportion = 0.3f;
    public float increaseThreshold = 0.5f;
    public float recovery = 0.5f;
    public float push = 1f;
    public float effectScale = 1f;
    [Range(0f, 1f)] public float drag = 0f;

    float baseValue;
    float initialValue;
    //float currentValue;
    float virtualValue;

    public void Initialize(float centralValue)
    {
        //this.centralValue = centralValue;
        initialValue = centralValue;
        baseValue = centralValue * baseIntensityProportion;
        //currentValue = baseValue;
        virtualValue = baseValue;
    }

    public float Update(float currentValue,float currentIntensity, float deltaTime)
    {
        if (!active) return currentValue;

        if (currentIntensity > increaseThreshold)
        {
            virtualValue += push * deltaTime * effectScale;
        }
        else
        {
            //currentValue = Mathf.Lerp(currentValue, baseValue, recovery * Time.deltaTime);
            virtualValue = Mathf.Max(baseValue, virtualValue - recovery * deltaTime * effectScale);
        }

        currentValue = Mathf.Lerp(currentValue, virtualValue, 1f - drag);

        return currentValue;
    }

    public float InitialValue
    {
        get { return initialValue; }
    }
}
