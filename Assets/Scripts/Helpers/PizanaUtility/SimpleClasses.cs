﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizanaUtility
{
    public abstract class IParams
    {

    }
    public class RequireStruct<T> where T : struct { }
    public class RequireClass<T> where T : class { }

    [System.Serializable]
    public class Pair<T1, T2>
    {
        public T1 first;
        public T2 second;

        public Pair(T1 first, T2 second)
        {
            this.first = first;
            this.second = second;
        }
    }
}
