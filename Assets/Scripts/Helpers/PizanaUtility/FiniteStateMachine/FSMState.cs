﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace PizanaUtility.FSM
{
    public abstract class FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType> : ScriptableObject
       where ControllerType : FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where StateType : FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where OngoingActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where InstantActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where TransitionType : FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where DecisionType : FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
    {


        [Header("On-State")]
        public OngoingActionType[] ongoingActions;

        [Header("On-Transitioning")]//It has sense to have overall transition actions that will be done in any case for this state
        public InstantActionType[] startStateActions;
        public InstantActionType[] endStateActions;

        [Header("Transitions")]
        public TransitionType[] transitions;

        private bool transiting = false;

        public void UpdateState(ControllerType controller)
        {
            transiting = false;

            for (int i = 0; i < transitions.Length; i++)
            {
                //The first successful transition ends the loop
                if (transitions[i].Assert(controller))
                {
                    transiting = true;
                    break;
                }

                //if (transitions[i].Assert(controller)) controller.TransitionToState(transitions[i].nextstate);
            }

            if (transiting) return;

            for (int i = 0; i < ongoingActions.Length; i++)
                ongoingActions[i].Act(controller);
        }

        public void EnterState(ControllerType controller)
        {
            for (int i = 0; i < startStateActions.Length; i++)
            {
                startStateActions[i].Act(controller);
            }
        }

        public void EndState(ControllerType controller)
        {
            for (int i = 0; i < endStateActions.Length; i++)
            {
                endStateActions[i].Act(controller);
            }
        }
    }
}
