﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://blogs.msdn.microsoft.com/simonince/2008/06/12/generics-the-self-referencing-generics-pattern/

namespace PizanaUtility.FSM
{
    public abstract class FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType> : MonoBehaviour
       where ControllerType : FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where StateType : FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where OngoingActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where InstantActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where TransitionType : FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where DecisionType : FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
    {
        public StateType currentState;
        public OngoingActionType[] generalActions;
        public bool AIActive { get { return enabled; } set { enabled = value; } }

        [Header ("Debug")]
        public bool debugMessages = true;

        private ControllerType thisCasted;

        public virtual void Awake()
        {
            thisCasted = (ControllerType)this;
        }

        public virtual void Update()
        {
            for (int i = 0; i < generalActions.Length; i++)
                generalActions[i].Act(thisCasted);

            currentState.UpdateState(thisCasted);//We hard cast in order to get exceptions if any
        }

        public void SetState(StateType nextState)
        {
            if (nextState != null)
            {
                currentState.EndState((ControllerType)this);
                currentState = nextState;
                currentState.EnterState((ControllerType)this);
            }
#if UNITY_EDITOR
            else if (debugMessages) Debug.LogError("FSMController: Trying to switch to a null state", this);

            //if (debugMessages) Debug.Log("FSMController: Switched to state: " + currentState.name);
#endif
        }
    }
}
