﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizanaUtility.FSM
{
    public abstract class FSMInstantAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType> 
        : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
            where ControllerType : FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
            where StateType : FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
            where OngoingActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
            where InstantActionType : FSMInstantAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
            where TransitionType : FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
            where DecisionType : FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
    {

    }

}
