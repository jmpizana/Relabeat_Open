﻿/*Made by José María Pizana, 2018
 * 
 * http://www.jmpizana.com
 */

using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using JetBrains.Annotations;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace PizanaUtility
{

    public static class GeneralExtensions
    {
        public static void AssignNotNull<T>(this T destiny, T source) where T : IParams
        {
            FieldInfo[] fields = source.GetType().GetFields();

            foreach (FieldInfo v in fields)
            {
                var value = v.GetValue(source);

                if (value == null) throw new UnityException("An IParams object was found to have null values when assigning it");

                if (!v.IsStatic)
                    v.SetValue(destiny, value);
            }
        }

        /// <summary>
        /// Compares folders to an array of non duplicate valid names. 
        /// </summary>
        /// <param name="folders"></param>
        /// <param name="validNames"></param>
        public static bool CompareFoldersWithNamesStrict(this DirectoryInfo[] folders, string[] validNames, bool caseSensitive = false)
        {
            List<string> usedNames = new List<string>(validNames.Length);

            if (!caseSensitive)
                for (int i = 0; i < validNames.Length; i++) validNames[i] = validNames[i].ToUpper();

            if (folders.Length != validNames.Length)
            {
                Debug.Log("Folders amount is different than names amount");
                return false;
            }

            for (int i = 0; i < folders.Length; i++)
            {
                string folderName = folders[i].Name;
                if (!caseSensitive) folderName = folderName.ToUpper();

                if (usedNames.Contains(folderName))
                {
                    Debug.Log("Duplicate folder found: " + folderName);
                    return false;
                }

                if (!validNames.Contains(folderName))
                {
                    Debug.Log("Non-permitted folder name found: " + folderName);
                    return false;
                }

                usedNames.Add(folderName);
            }

            return true;
        }

        public static DirectoryInfo GetByName(this DirectoryInfo[] folders, string name, bool caseSensitive = false)
        {
            string[] folderNames = new string[folders.Length];
            folders.GetNames().CopyTo(folderNames, 0);

            if (folders == null || folders.Length == 0)
                throw new UnityException("Input folders are null or empty");

            if (!caseSensitive)
            {
                for (int i = 0; i < folderNames.Length; i++)
                {
                    folderNames[i] = folderNames[i].ToUpper();
                }
                name = name.ToUpper();
            }
            //Debug.Log(name);
            //folderNames.ForEach((string s) => Debug.Log(s));

            for (int i = 0; i < folders.Length; i++)
            {
                if (folderNames[i] == name) return folders[i];
            }

            Debug.Log("No folder found with requested name");
            return null;
        }

        public static string[] GetNames(this DirectoryInfo[] folders)
        {
            string[] names = new string[folders.Length];
            for (int i = 0; i < names.Length; i++)
            {
                names[i] = folders[i].Name;
            }

            return names;
        }
    }


}
